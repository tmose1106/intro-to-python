******************
Practical Projects
******************

This site includes some "mini-projects" for
Some mini-project will include a ``requirements.txt`` file specific to that
project. After entering a virtual environment and installing the dependencies,
the project can be ran by running the included ``.py`` file.

.. note::

  For more information, please read the appendix sections on
  `installing dependencies`_ and `virtual environments`_.


.. toctree::
  :caption: Index
  :maxdepth: 1

  table_generator
  web_scraper
  card_game

.. _`installing dependencies`: ../appendix/package_install.rst
.. _`virtual environments`: ../appendix/virtual_environments.rst

Other Resources
===============

Below I have some praise for Python learning resources created by other
individuals.

Drawing using ``turtle``
------------------------

Python educator `Christian Thompson`_ has a short series on 2D animation using
Python's built-in ``turtle`` module. In this tutorial, he attempts to draw
a functioning traffic light. In his first video he does so using a simple
procedural programming. In the second part of the series, he comes back to
modify the program to be class-oriented. Here is a `link to the playlist`_.

I personally think this a great example for trying to understand why classes
are so important, as well as a look at using Python's built-in modules to
solve a seemingly non-trivial task.

.. note::

  If you are not familiar with turtle graphics, I would suggest skimming the
  `Wikipedia page`_ and looking at this `short tutorial`_ on using the
  ``turtle`` module.

  For more information, you can look at the `documentation`_ for ``turtle``
  directly.

.. hint::

  Christian also has some other interesting tutorials on his
  `personal website`_ and `YouTube channel`_.

.. _`Christian Thompson`: http://www.christianthompson.com/
.. _`documentation`: https://docs.python.org/3/library/turtle.html
.. _`link to the playlist`: https://www.youtube.com/playlist?list=PLlEgNdBJEO-nBamgXaPH5nshPu7AJWJQt
.. _`personal website`: http://www.christianthompson.com/
.. _`short tutorial`: https://opentechschool.github.io/python-beginners/en/simple_drawing.html
.. _`Wikipedia page`: https://en.wikipedia.org/wiki/Turtle_graphics
.. _`YouTube channel`: https://www.youtube.com/channel/UC2vm-0XX5RkWCXWwtBZGOXg
