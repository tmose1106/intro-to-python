***************
Table Generator
***************

In this mini-project, we will be creating a script which converts a
comma separate values (*.csv*) file to table formatted in reStructuredText
(*.rst*). This is a task that can be quite tedious to do by hand and can be
easily automated.

Modules Used
============

`csv`_
  A standard library module for handling `comma separate values`_ files. More
  specifically, we will be using the

  `reader`_ (csvfile)
    An object to iterate over rows in a comma separated values file. *csvfile*
    is an open file descriptor.

`sys`_
  A standard library module to work with interpreter specific features.

  `argv`_
    A list of arguments passed to the interpreter on the command line.

  `exit`_ ([arg])
    Exit the Python interpreter. *arg* can be an integer from 0-127 or any
    type of object. If an object, it will attempt to print it.

.. _`argv`: https://docs.python.org/3/library/sys.html#sys.argv
.. _`comma separate values`: https://en.wikipedia.org/wiki/Comma-separated_values
.. _`csv`: https://docs.python.org/3/library/csv.html
.. _`exit`: https://docs.python.org/3/library/sys.html#sys.exit
.. _`reader`: https://docs.python.org/3/library/csv.html#csv.reader
.. _`sys`: https://docs.python.org/3/library/sys.html

Code
====

The code below is quite terse, but may not be entirely obvious.

.. literalinclude:: ../_static/examples/projects/table_generator/csv2rst.py
  :language: python3
  :caption: csv2rst.py
  :linenos:

Sample Output
=============

.. literalinclude:: ../_static/examples/projects/table_generator/sample.csv
	:caption: sample.csv
	:linenos:

.. literalinclude:: ../_static/examples/projects/table_generator/sample.rst
	:language: restructuredtext
	:caption: sample.rst
	:linenos:

Notes
=====

This script is quite simple, and only provides very basic functionality. It
makes a few assumptions, i.e. the first row in the csv file is a line with
comma separate title values for each column.
