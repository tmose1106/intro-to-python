***********
Web Scraper
***********

In this mini-project we are going to create a **web scraper**. The purpose of
the web scraper is to fetch the contents of a webpage, and then gather
information from it that may be important to us. In this tutorial, we are
going to design a web scraper for gathering data from `Bandcamp`_.

This example is intended to show students how to use 3rd party dependencies
as well as procedural programming, more generally know as scripting in Python.

.. _`Bandcamp`: https://bandcamp.com/

Dependencies
============

In this mini-project, we will be using a few dependencies:

* `BeautifulSoup`_ parses HTML documents
* `html5lib`_ generates a "document tree" for BeautifulSoup
* `requests`_ request web pages from a URL

.. note::

  We will not be using ``html5lib`` directly, but rather as an optional
  dependency of ``BeautifulSoup``.

.. _`BeautifulSoup`: https://www.crummy.com/software/BeautifulSoup/
.. _`html5lib`: https://github.com/html5lib/html5lib-python
.. _`requests`: http://docs.python-requests.org/en/master/

Code
====

In the code below, we ask the user to provide a URL as an argument via
the command line. We then get the web page this URL points to and parse it.
Then, we proceed to take information out of the parsed web page and present it
to the user as a nicely formatted table.

.. literalinclude:: ../_static/examples/projects/web_scraper/bandcamp_scraper.py
  :language: python3
  :caption: main.py
  :linenos:

Sample Output
=============

Running on `"Is Survived By"`_ by `Touché Amoré`_

::

  Title: Is Survived By
  Artist: touche amore
  Date: 2013-09-24

  01 Just Exist 02:17
  02 To Write Content 02:57
  03 Praise / Love 01:01
  04 Anyone / Anything 02:39
  05 DNA 02:08
  06 Harbor 03:04
  07 Kerosene 01:42
  08 Blue Angels 01:31
  09 Social Caterpillar 03:03
  10 Non Fiction 03:05
  11 Steps 02:38
  12 Is Survived By 03:30

Running on the Counter Intuitive Records release of `"best buds"`_ by
`Mom Jeans`_

::

  Title: Mom Jeans. - Best Buds
  Artist: Counter Intuitive Records
  Date: 2016-07-03

  01 Death Cup 04:36
  02 Danger Can't 03:34
  03 Movember (ft. Sarah Levy) 03:37
  04 Edward 40hands 04:26
  05 *Sobs Quietly* 01:58
  06 Poor Boxer Shorts 03:53
  07 Remy's Boyz 03:00
  08 Girl Scout Cookies 03:45
  09 Scott Pilgrim vs. My GPA 03:59
  10 Vape Nation 03:04

The above example has a bit of an issue. Some record labels on Bandcamp put
the label name instead of the artist, which makes our script slightly less
useful.

.. _`"best buds"`: https://counterintuitiverecords.bandcamp.com/album/mom-jeans-best-buds
.. _`"Is Survived By"`: https://toucheamore.bandcamp.com/album/is-survived-by
.. _`Mom Jeans`: https://momjeansband.com/
.. _`Touché Amoré`: https://en.wikipedia.org/wiki/Touch%C3%A9_Amor%C3%A9

Program Notes
=============

The uses for web scrapers are almost limitless. Anywhere that there is a
dynamic website, there can be a use for a web scraper. A common example would
be to create a web scraper for a shopping site (Amazon, Newegg, etc.) and
recording the prices in a database to see when a product was the least
expensive.

It is also worth noting that creating a web scraper is not always the most
efficient method for this type of work. If the style of a web page is changed,
the web scraper will likely need to be adapted.

Some websites provide an Application Programming Interface (API) for accessing
the site's useful data. If you had to decide between creating a web scraper or
"wrapping" the website's API, it may be more wise to pursue the latter.
