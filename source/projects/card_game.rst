*********
Card Game
*********

In this project, we create a text-based version of the classic game Go Fish.
This version includes prompts for the human player, naive computer player
AI and not a whole lot more.

The intent of this project is to show how using the correct language features
and data structures can create a very compact and concise project. This
script uses no external libraries, and is composed of about ~150 LoC,
including comments.

.. note::

	The rules I chose to use for this version of the game are based on `pagat.com's set of rules`_.

.. _`pagat.com's set of rules`: https://www.pagat.com/quartet/gofish.html

Example Code
============

.. literalinclude:: ../_static/examples/projects/card_game/go_fish.py
    :language: python3
    :linenos:

Project Notes
=============

There are a few takeaways here:

* List, set and dictionary comprehensions are a beautiful feature
* Solid operator overloads (as seen on sets) can be very clean and
  extremely concise when used with comprehensions
* Subclassing can produce extremely DRY code

Future Suggestions
==================

* Right now the ``play_turn`` is quite long, which is not ideal for
  readability. Try breaking this method into smaller, composable chunks.
* Perhaps try making other games using these same ideas. For example,
  writing an Uno clone might be an interesting personal project?

.. note::

    Anything beyond a simple text-based can become very complex very
    quickly! A lot more goes into making a game with graphics than
    you might think.
