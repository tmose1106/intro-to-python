***************************
Welcome to Intro to Python!
***************************

This project is dedicated to teaching the `Python programming language`_ from
front to back through concise yet intuitive examples. The site itself has been
designed as four separate parts:

* A syntax tutorial for learning the building blocks that make up the language
* Practical "mini-projects" using both built-in and third-party packages
* Sets of challenge problems which can be completed to help reinforce
  discussed topics as well as build critical thinking skills
* An appendix which goes into depth with topics not specific to Python

.. toctree::
  :maxdepth: 2
  :hidden:

  tutorials/index
  projects/index
  challenges/index
  appendix/index

.. _`Python programming language`: https://en.wikipedia.org/wiki/Python_(programming_language)

About This Workshop
===================

This site is being developed for the Rutgers University `Novice to Expert
Coding Club`_ (N2E) Python workshop. It is intended to act as "slides" to aid
in teaching Python at lecture workshops hosted by N2E, as well as a resource
which can be directly used by students as a brief and straight-forward
reference.

=================== =================================================
Live Website        `tmose1106.gitlab.io/intro-to-python`_
Site Source Code    `gitlab.com/tmose1106/intro-to-python`_
Example Source Code `gitlab.com/tmose1106/intro-to-python-examples`_
=================== =================================================

.. _`Novice to Expert Coding Club`: http://n2ecodingclub.rutgers.edu/
.. _`tmose1106.gitlab.io/intro-to-python`: https://tmose1106.gitlab.io/intro-to-python/index.html
.. _`gitlab.com/tmose1106/intro-to-python`: https://gitlab.com/tmose1106/intro-to-python
.. _`gitlab.com/tmose1106/intro-to-python-examples`: https://gitlab.com/tmose1106/intro-to-python-example

About the Author
================

This workshop is being developed by Ted Moseley, an everyday student of
Rutgers New Brunswick. Beginning in Spring of 2015, Ted was able to teach
himself Python through the plethora of freely available resources available
online. If he could do it, so can you!

References
==========

This website is the product of compiling together several free (`as in
freedom`_) resources across the internet, including (but not limited to):

`Official Python Documentation`_
  Documentation describing the Python language and the entire standard
  library. While it is complete, it is quite dense and is not beginner
  friendly.

`Automate The Boring Stuff`_
  A free and extremely popular book describing how to get up an running in
  Python through examples. The first half of the book focuses on Python syntax
  while the second half focuses on a few of Python's use cases for automating
  certain tasks and making life easier.

`Think Python (2nd Edition)`_
  A free and complete description of Python 3's syntax. Highly recommended by
  the Python community.


`Style Guide for Python Code`_
  A document describing common code formatting behaviors for Python.

`The Zen of Python`_
  A short document explaining what Python is all about. Typically used to
  determine if a piece of code is *Pythonic*.

.. _Automate The Boring Stuff: https://automatetheboringstuff.com/
.. _as in freedom: https://en.wikipedia.org/wiki/Free_software
.. _Official Python Documentation: https://docs.python.org/3/index.html
.. _Style Guide for Python Code: https://www.python.org/dev/peps/pep-0008/
.. _The Zen of Python: https://www.python.org/dev/peps/pep-0020/
.. _Think Python (2nd Edition): http://greenteapress.com/wp/think-python-2e/

Licensing
=========

This project is released under the terms of the `Creative Commons Attribution
4.0 International`_ license. The example code is released under the terms of
the `MIT/Expat`_ license. For more information, read the *LICENSE.txt* and
*source/_static/examples/LICENSE.txt* files.

.. _`Creative Commons Attribution 4.0 International`: http://creativecommons.org/licenses/by/4.0/
.. _`MIT/Expat`: https://spdx.org/licenses/MIT.html
