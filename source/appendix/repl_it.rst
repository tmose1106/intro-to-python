****************************
Learning Python with repl.it
****************************

`repl.it`_ is a website which hosts free development environments for several
languages accessible through your web browser. With an account, you can save
your environment for future use and share your work with others. However, an
account is not necessary to get started. Using this site, you can do a
majority of what will be described throughout this workshop.

.. _`repl.it`: https://repl.it/

Start a Python REPL
===================

To start your first Python REPL, visit the `Python 3 REPL`_ page. Here, .
will greeted with three main views:

* The terminal view, which will have a Python REPL open for typing lines of
  code.

  .. image:: ../_static/images/repl_it_terminal.png

* The explorer view, for creating and editing files and folders.

  .. image:: ../_static/images/repl_it_files.png

* The editor view, where you can edit text files

  .. image:: ../_static/images/repl_it_editor.png

.. _`Python 3 REPL`: https://repl.it/languages/python3