******************************
Utilizing Virtual Environments
******************************

What is a Virtual Environment?
==============================

A virtual environment is shell environment with restricted Python dependencies
installed to aid in developement. It essentially hides all of the system's
install dependencies and only provide the bare necessities.

The virtual environment is used specifically for developement. It allows the
developer to control dependencies and their version without altering their
system-wide dependencies.

For example, on a Windows machine you can view all of your installed Python
dependencies using the ``pip list`` command:

::

  PS C:\Users\tmose\Projects\project_name> py -m pip list --format=columns
  Package          Version
  ---------------- ---------
  certifi          2017.11.5
  chardet          3.0.4
  cx-Freeze        5.1.1
  flake8           3.5.0
  idna             2.6
  mccabe           0.6.1
  pew              1.1.2
  pip              9.0.1
  pipenv           9.0.1
  psutil           5.3.1
  py2exe           0.9.2.2
  pycodestyle      2.3.1
  pyflakes         1.6.0
  requests         2.18.4
  setuptools       28.8.0
  urllib3          1.22
  virtualenv       15.1.0
  virtualenv-clone 0.2.6

Now lets say we wish to have a clean developement environment. We would create
a virtual environment, and when we activate the virtual environment, our
dependencies would look something like this:

::

  (env-3_6_4) PS C:\Users\tmose\Projects\project_name> pip list --format=columns
  Package    Version
  ---------- -------
  pip        9.0.1
  setuptools 28.8.0

Creating a Virtual Environment
==============================

All version of Python after 3.4 provide a command called ``venv`` for creating
virtual environments. It can be used across platforms as follows:

::

  [insert-python-executable] -m venv [insert-virtual-env-folder-name]

Here we see two somewhat arbitrary values:

* The Python executable will be ``py`` on Windows or ``python3`` on UNIX-like
  systems.
* The virtual environment folder name can be anything you want it to be,
  though you should probably standardize it.

This method will work on all operating systems.

Preparing PowerShell for Virtual Environments
=============================================

In order to use virtual environments in Windows PowerShell, we must alter the
user's execution policy. This means we are allowed to run scripts which are
no verified by Microsoft. This can be seen as a minor security hazard, but it
is unlikely.

To do so, we must do the following steps:

* Open a PowerShell with admin rights (Right click start menu > "Windows
  PowerShell (Admin)")
* Run the ``Set-ExecutionPolicy`` command with the proper options.
* Run the ``Get-ExecutionPolicy`` command to verify the previous command
  worked as intended.

After you open an admin PowerShell, you should run the following command to
perminantly set the policy to unrestricted:

::

  Set-ExecutionPolicy -Scope CurrentUser -ExecutionPolicy Unrestricted

We can check that it worked by running:

::

  Get-ExecutionPolicy

Now we can use virtual environments whenever we open a PowerShell. You can
read more about execution policies in `Windows PowerShell documentation`_.

.. note::

  If you are uncomfortable with the leaving the execution policy unrestricted
  (for whatever reason) then you can set it back using the following command

  ::

    Set-ExecutionPolicy -Scope CurrentUser -ExecutionPolicy Restricted

.. _Windows PowerShell documentation: https://docs.microsoft.com/en-us/PowerShell/module/microsoft.PowerShell.core/about/about_execution_policies?view=PowerShell-6&viewFallbackFrom=PowerShell-Microsoft.PowerShell.Core

Entering a Virtual Environment
==============================

In order to enter the virtual environment, we must run the activation script
that was generated inside of the virtual environment folder.

Windows Method
--------------

::

  .\[insert-virtual-env-folder-name]\Scripts\Activate.ps1

Now at the begining of your prompt, there should be a greet portion with the
name of your virtual environment encased in parenthesis.

UNIX-like Method
----------------

::

  source ./[insert-virtual-env-folder-name]/bin/activate

Your prompt will now be preceeded by the name of your virtual environment in
parenthesis.

Using the Environment
=====================

Now we can use Pip as explained in `Installing 3rd Party Packages`_. Many
times projects will make this process easy by including a ``requirements.txt``
file which lists all of the required dependencies. This file can also be used
by Pip to install our dependencies by running the command below:

::

  [insert-python-executable] -m pip install -r requirements.txt

Now these clean and versioned dependencies will be installed. When the command
is done, they are available inside of the virtual environment and the project
can be run normally.

.. _Installing 3rd Party Packages: ./package_install.html
