**************************
Stringing Together Strings
**************************

Generally speaking, strings are one of the most widely used type in Python.
Strings also possess many features which require different levels of prior
knowledge. For this reason, they are discussed in this appendix section.

A Hidden Container
==================

While it may not appear obvious at first, the ``str`` type is actually a
"hidden" container of sorts. At its base, a string is essentially a list of
singular characters. Below is an example using the ``for`` loop:

.. code-block:: pycon3

  >>> my_string = "Hello, World!"
  >>> for a_character in my_string:
  ...     print(a_character)
  ...
  H
  e
  l
  l
  o
  ,

  W
  o
  r
  l
  d
  !

Methods of Strings
==================

Strings have many functions which can be run on them. Below is a list of some
more prominent examples.

``endswith(suffix)``
  Return ``True`` if the string ends with the string *suffix*.

  .. code-block:: pycon3

    >>> my_string = "This is the end."
    >>> my_string.endswith("end")
    False
    >>> my_string.endswith("end.")
    True

``find(sub)``
  Return the index of *sub* in the string. Return ``-1`` if not found.

``title()``
  Capitalize the first letter of each word.

  .. code-block:: pycon3

    >>> my_string = "eden of the east"
    >>> my_string.title()
    "Eden Of The East"

.. note::

  How these special functions called *methods* are defined will be discussed
  in the `Classifying Classes`_ tutorial.

A list of all methods available for strings can be seen in the Offical Python
`String Documentation`_.

.. _Classifying Classes: ../tutorials/classes.html
.. _String Documentation: https://docs.python.org/3/library/stdtypes.html#string-methods

Formatting Strings
==================

Python 3 includes a new string formatting syntax using the ``format()``
method. Quite generally it can be used like below:

.. code-block:: python3

  my_string = "{}, {}!"

  print(my_string.format("Hello", "World"))

::

  Hello, World!

You may also see examples where the `C-like printf`_ is used. This was the way
to format strings in the previous versions of Python and has since been
replaced. For this reason it will not be discussed.

An extremely helpful resource for understanding Python string formatting is
`pyformat.info`_. They provide examples for all of the different features of
Python's string formatting in the new syntax (``format()`` method) along
with how the same output could be produced using the old syntax (printf
syntax).

.. _C-like printf: https://en.wikipedia.org/wiki/Printf_format_string
.. _pyformat.info: https://pyformat.info/

Special String Types
====================

There are a few different types of *special* strings which modify the syntax
for creating strings. The way these strings are defined is by placing a single
character in front of the starting quote of the string.

f-strings
---------

A new type of strings with special formatting abilities. To define a f-string,
one must only put a lower ``f`` in front of the string starting quote. This
allows the programmer to place a single statement inside of curly braces, and
will be executed when the string is printed.

.. code-block:: pycon3

  >>> f"2 + 2 = {2 + 2}"
  '2 + 2 = 4'

.. note::

  These f-strings are new to Python 3.6 and will not work in older versions of
  Python. This should only be a consideration if you wish to distribute your
  code to others, who may have different versions of Python installed.

Raw Strings
-----------

Strings that represent exactly what they contain. To define a raw string, one
must only put a lower ``r`` in front of the string starting quote.

.. code-block:: pycon3

  >>> "Get goin\'!"
  "Get goin'!"
  >>> r"Get goin\'!"
  "Get goin\\'!"

Unicode Strings
---------------

Strings that may contain special unicode characters. To define a unicode
string, one must only put a lower ``u`` in front of the string starting quote.
By doing so, this allows us to define unicode characters using different
syntaxes. Two such examples can be seen in the example below.

.. code-block:: pycon3

  >>> u"\u00B5"
  'µ'
  >>> u"\N{MICRO SIGN}"
  'µ'

We can also include the base characters like any other string.

.. code-block:: pycon3

  >>> print(u"\u00C6nema")
  Ænema

A full list of characters defined in the unicode standard along with their
different representations can be found on this `Wikipedia page`_.

.. _Wikipedia page: https://en.wikipedia.org/wiki/List_of_Unicode_characters
