************************
Installing A Text Editor
************************

A text editor is a tool which allows one to enter plain text and save said
input into a plain text file. Text editors can range from simple programs
(I.E., Notepad on Windows) to full integrated developement environments
(`IDEs`_) cattering towards a single programming language.

For the sake of this workshop, I will be recommending a text editor known as
`Atom`_. It is a free and open text editor created by a team at GitHub.

.. _Atom: https://atom.io/
.. _IDEs: https://en.wikipedia.org/wiki/Integrated_development_environment

Installation
============

For the most part, installation should also be quite simple. Just select the
correct installer from the homepage and follow the installation wizard.

Some Keyboard Tips
==================

Below are some common keyboard shortcuts for Windows and GNU/Linux systems.
They may differ slightly on Mac OSX, but they can all be found relatively
easily by looking through the menus, using Atom's command palette or entering
the *Keybindings* menu under *Edit->Preferences*.

``Ctrl-S``
  Save File

``Ctrl-Shift-S``
  Save File As

``Ctrl-N``
  Open a New File

``Ctrl-W``
  Close a Window

``Ctrl-,``
  View Settings/Preferences Menu

``Ctrl-Shift-P``
  Toggle Command Palette

Plugins
=======

Here is a list of some non-mandatory plugins for Atom that may make your life
easier. They can be installed through Atom's graphical interface by using the
key

`language-restructuredtext`_
  Text highlighting for `reStructuredText`_ in Atom

`open-terminal-here`_
  A plugin for opening a command line in the directory of the current project


.. _language-restructuredtext: https://atom.io/packages/language-restructuredtext
.. _open-terminal-here: https://atom.io/packages/open-terminal-here
.. _reStructuredText: http://docutils.sourceforge.net/rst.html
