*****************************
Installing 3rd Party Packages
*****************************

When you are programming, you do not want to write everything yourself when
someone has already created a solution for your use. This is where installing
3rd Party packages comes in.

Using Pip
=========

`Pip`_ is the default tool used to install 3rd Party Python packages. Pip
finds packages that are hosted on the `Python Packaging Index`_ (AKA PyPi).
At the time of writing, there are 125,000+ freely packages available on PyPi.

Python typically comes with Pip pre-installed. It can be used as follows:

On Windows
----------

::

	py -m pip install [package-name]

On OSX and GNU/Linux
--------------------

::

	pip3 install [package-name]

.. _Pip: https://pip.pypa.io/en/stable/
.. _Python Packaging Index: https://pypi.python.org/

Other Pip Commands
==================

Pip also includes many other commands. Below are a selected few that might
be important:

``freeze``
	Display a list of installed packages along with their version numbers
	*frozen* for easy *requirements.txt* creation.

``help``
	Display a usage message and most of the available options for pip.

``install -r [requirements-file]``
	Install packages listed inside of the requirements file.

``install --upgrade``
	Check for newer version of a package and install it if found.

``list``
	Display a list of installed packages.

``uninstall``
	Uninstalled a package from the current environment

Installing Manually
===================

If a package you wish to use is not available on PyPi but the source code is
available online (especially on sites like GitHub or Sourceforge), you can
still install the package. Python provides a method for installing packages
that use a *setup.py* file.

.. note::

	If you have never installed a package manually, make sure that the
	``setuptools`` package is installed using Pip.

In this example, we will install the Click_ package which can be found on
GitHub. We do so by downloading the source code, running the *setup.py* file
with the ``install`` argument and now the package will be installed globally,
meaning it can now be imported from any script.

On Windows
----------

::

	py setup.py install

On OSX and GNU/Linux
--------------------

::

	python3 setup.py install

.. _Click: http://click.pocoo.org/
