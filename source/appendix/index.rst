*******************
Additional Appendix
*******************

The appendix includes several pages discussing topics related to the usage of
Python and can improve the student's overall learning experience. However,
these topics are not mandatory to know in order to proclaim oneself a Python
master. Typically these topics are beyond the scope of the syntax tutorial.

.. toctree::
  :caption: Index
  :maxdepth: 1

  repl_it
  install_editor
  command_line
  strings
  package_install
  virtual_environments
  package_create
  restructuredtext
