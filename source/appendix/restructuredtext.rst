*********************************
Documenting with reStructuredText
*********************************

Python uses `reStructuredText`_ as its standard markup language for writing
documentation. In other words, reStructuredText allows us to create formatted
documents using plain text and convert the plain text documents in HTML, PDF,
etc.for distribution over the internet.

.. hint::

	Try clicking the *View page source* link at the top of the page to see the
	raw reStructuredText document.

reStructuredText is also commonly called **reST** and it typically uses a
``.rst`` file extension.

.. note::

	If you are familiar with `Markdown`_, then you will also be comfortable with
	reStructuredText. It is a very similar idea.

.. _`Markdown`: https://www.markdownguide.org/
.. _`reStructuredText`: http://docutils.sourceforge.net/rst.html

Writing reST Documents
======================

There are a plethora of resources online for learning how to write
reStructuredText documents. For those who would like to see a brief
introduction, check out the Docutils `Quick Reference`_. For a more complete
overview, you can skim the `Markup Specification`_.

.. _`Markup Specification`: http://docutils.sourceforge.net/docs/ref/rst/restructuredtext.html
.. _`Quick Reference`: http://docutils.sourceforge.net/docs/user/rst/quickref.html

Converting reST to Other Formats
================================

For individual documents, one can use the command line tools included with
the Python `Docutils`_ package. Docutils is a parser for the reST format,
allowing reStructuredText to be converted into more useful formats. For
example, we can convert a reST formatted file called ``rest.rst`` to modern
HTML (a webpage) called *rest.html*. We may do so using the provided
*rst2html5* command and passing it the two file names as arguments on the
command line.

::

	rst2html5 rest.rst rest.html

Now we may open *rest.html* in our web browser and see the output of our
reStructuredText document.

.. note::

	For this example to work, you need to install Docutils using `Pip`_. You
	will also need the Python scripts directory added to your system path.

.. _`Docutils`: http://docutils.sourceforge.net/
.. _`Pip`: ./package_install.html

Handling Documentation with Sphinx
==================================

While converting singular documents is all well and good, we need a framework
to make writing detailed documentation more viable. The standard tool for this
is `Sphinx`_. Sphinx is a command line tool which makes writing documentation
easy. That is why Sphinx is used to write this website and many others all
across the internet.

You can begin looking into Sphinx by following the `Sphinx Quickstart`_ which
guides one through the process of creating a simple Sphinx project.

.. _`Sphinx`: http://www.sphinx-doc.org/en/master/
.. _`Sphinx Quickstart`: http://www.sphinx-doc.org/en/master/usage/quickstart.html
