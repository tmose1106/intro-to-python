*************************
Usage of the Command Line
*************************

This tutorial is meant to act as a quick reference for very basic handling
of the Windows PowerShell and the terminal on Mac OSX and GNU/Linux.

What You Need To Know
=====================

Using text-based interfaces like PowerShell (on Windows) and Bash (on
UNIX-Like machines) center around the idea of working with files and
directories (AKA folders). The idea of files and directories can be condensed
into the idea of a **path**: a string of characters representing a directory
or file.

Paths are defined differently on different systems. On Windows directories
are separated using the '\\' character, while UNIX-like systems use the '/'
character. Between these separators are the names of directories, leading to
a directory or file.

For example, on Windows we may have the path
``C:\\Users\\tmose\\Documents\\Python\\hello.py`` which defines a Python
script in the ``Python`` folder, in the ``Documents`` folder in the user's
directory.

Windows PowerShell
==================

The current default command line interface for Windows is the `PowerShell`_.

.. _`PowerShell`: https://en.wikipedia.org/wiki/PowerShell

PowerShell vs. Command Prompt
-----------------------------

In Windows, I suggest using PowerShell over the Command Prompt (cmd.exe) since
it is seemingly deprecated in Windows 10, and it has more features like text
highlighting.

Commands
--------

cd [*DIR\_PATH*]
  Change the current directory to directory *DIR\_PATH*

cls
  Clear the terminal screen

dir
  List the name of a directory (folder) and information about it's contents

mkdir *DIR\_PATH*
  Create a new directory (folder) with name *DIR\_PATH*

rm *FILE\_PATH*
  Remove the file at *FILE\_PATH*

rmdir *DIR\_PATH*
  Remove the directory at *DIR\_PATH*

Example
-------

::

  PS C:\Users\n2epython> dir


      Directory: C:\Users\n2epython


  Mode                LastWriteTime         Length Name
  ----                -------------         ------ ----
  d-r---        10/7/2017   1:34 PM                Contacts
  d-r---        10/7/2017   1:34 PM                Desktop
  d-r---        10/8/2017  11:50 AM                Documents
  d-r---        10/7/2017   1:34 PM                Downloads
  d-r---        10/7/2017   1:34 PM                Favorites
  d-r---        10/7/2017   1:34 PM                Links
  d-r---        10/7/2017   1:34 PM                Music
  d-r---        10/7/2017   1:34 PM                Pictures
  d-r---        10/7/2017   1:34 PM                Saved Games
  d-r---        10/7/2017   1:34 PM                Searches
  d-r---        10/7/2017   1:34 PM                Videos
  -a----        10/8/2017  11:53 AM           1669 .bash_history

  PS C:\Users\n2epython> mkdir .\Documents\Python\
  PS C:\Users\n2epython> cd .\Documents\Python\
  PS C:\Users\n2epython\Documents\Python> cd ..\..
  PS C:\Users\n2epython>


Bash on OSX and GNU/Linux
=========================

`UNIX-Like`_ systems provide a plethora of different command line tools and
options for customization. But typically most of these systems ship with some
kind of default terminal program and the `GNU Bash`_ shell.

.. _`GNU Bash`: https://en.wikipedia.org/wiki/Bash_(Unix_shell)
.. _`UNIX-Like`: https://en.wikipedia.org/wiki/Unix-like

Commands
--------

cd [*DIR\_PATH*]
  Change to directory *DIR\_PATH*

clear
  Clear the terminal screen

env
  Print all available environment variables

ls [*DIR\_PATH*]
  List the contents of a directory

mkdir *DIR\_PATH*
  Make a directory with name *DIR\_PATH*

pwd
  Print the name of the current working directory

rm *FILE\_PATH*
  Remove a file at *FILE\_PATH* (add a *-r* flag to remove non-empty
  directories)

Environment Variables
---------------------

HOME
  The current user's home directory

PATH
  The list of directories where program binaries are searched for

SHELL
  The current interpreter shell being used

Example
-------

::

  [tedm1@archbook ~]$ ls
   Desktop     Downloads   Pictures   Scripts  'VirtualBox VMs'
   Documents   Games       Projects   Sources   VPN
  [tedm1@archbook ~]$ mkdir Documents/Python
  [tedm1@archbook ~]$ cd Documents/Python/
  [tedm1@archbook ~]$ pwd
  /home/tedm1/Documents/Python
  [tedm1@archbook Python]$ cd ../..
  [tedm1@archbook ~]$


References
==========

* https://ryanstutorials.net/linuxtutorial/
* https://www.computerhope.com/issues/chusedos.htm
