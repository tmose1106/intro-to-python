*******************
Interpreting Python
*******************

Python is an interpreted language, meaning that it has no compilation step.
There is a program called the **interpreter** which reads lines of Python
code and uses them to tell the computer what to do. We as programmers do
not interact with the interpreter directly. Instead, there are two ways
that we interact with the Python `interpreter`_:

* Starting a REPL session and typing lines of code one-by-one
* By feeding it a script containing Python code

We will now discuss both of these methods.

.. _`interpreter`: https://en.wikipedia.org/wiki/Interpreter_(computing)
.. _`Integrated Development and Learning Environment`: https://docs.python.org/3/library/idle.html

Accessing IDLE
==============

For the sake of simplicity, we will be using Python's included `Integrated
Development and Learning Environment`_ (IDLE) program for this tutorial. This
program allows us to easily open a REPL session or write a Python script.

Windows
-------

After following the previous download instructions, you can open a Window with
a REPL by:

* Press the Windows key to open the start menu
* Being typing *IDLE* and select the "IDLE (Python 3.7 64-bit)" app

.. image:: ../_static/images/idle_launch_windows.png

OSX
---

After following the Mac download instructions, IDLE should be added to your
Applications folder in Finder. Just search *IDLE* using Spotlight.

GNU/Linux
---------

On a GNU/Linux based system, the ``idle3`` executable will be added to your
path. Launch this however your distribution sees fit.

Starting a REPL with IDLE
=========================

The easiest way for a beginner to learn basic Python is through Python's
included `read-eval-print loop`_ (REPL). The REPL allows for users to interact
with the Python interpreter by typing code line-by-line and viewing its
output. It is meant to act as a "scratch pad" so-to-speak for trying out
simple Python statements. By default, opening IDLE will start a new REPL
session, as shown below.

.. image:: ../_static/images/idle_window.png

For your first Python program, we will continue the age-old tradition of
writing a Hello World program.

.. code-block:: pycon3

  >>> print("Hello, World!")
  Hello, World!

There are a few important things to note about the above code:

* When a line starts with ``>>>``, that denotes that we can type in a new
  statement.
* A line without any starting characters represents output from a statement
  that was run by the interpreter.
* We are using a built-in **function** called ``print`` which takes one
  or more arguments (inputs), and prints them to the screen. Arguments are
  comma separated within a pair of parenthesis following the name of the
  function. There is **NO** space between the function name and the opening
  parenthesis.
* The argument that we pass is a simple 13 character string, which is denoted
  by using double quotes to begin and end the string.

We will discuss all of these topics more in depth throughout the syntax
tutorial.

.. _read-eval-print loop: https://en.wikipedia.org/wiki/Read%E2%80%93eval%E2%80%93print_loop

Writing a Script with IDLE
==========================

IDLE can also act as a simple Python `Integrated Development Environment`_
(IDE), allowing the user to write lines of Python code to a text file which
we call a **script**. All Python scripts end with a **.py** extension.

We can start a new script in IDLE by accessing *File > New File (Ctrl-n)*.
This will open a blank editor window. First save the file by accessing
*File > Save (Ctrl-s)* and saving it somewhere you can remember.

.. image:: ../_static/images/idle_editor.png

Once your script is ready, you can run it by accessing *Run > Run Module
(F5)*. This will show the result of your script in the REPL window.

.. image:: ../_static/images/idle_run_script.png

Notice, if we want to run multiple lines of code, we simply write more code
on the next line. The script will be read from top-to-bottom and executed in
that order.

.. _`Integrated Development Environment`: https://en.wikipedia.org/wiki/Integrated_development_environment

Script versus REPL
==================

To a beginner, the difference between writing a script and using the REPL may
not seem apparent. The rule of thumb is:

* Use the REPL for testing and learning what code does
* Write a script any other time, especially to save your work

For example, you can share your work with others with relative ease when you
write a script. The file be shared over email, cloud storage, a Git
repository, etc. Furthermore, writing a script adds all of the benefits of
using a text editor including syntax highlighting and breaking code into
multiple smaller files to name a few.

A Disclaimer
============

When you are first learning programming, it is imperative the one remains
curious.

If you ever have a question along the lines of "What happens if I..." or
"What will ... do?", try it out. That is the intended use-case for the REPL!

On the other hand, if you ever have a question like "How can I ...?" you can
always use your favorite search engine to find out. And don't forget to use
well thought out `search queries`_.

.. _search queries: http://www.googleguide.com/category/query-input/
