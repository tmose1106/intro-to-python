***************
Using Operators
***************

An `operator`_ is a syntax element which acts upon two values. For example,
the addition operator can be used like so:

.. code-block:: python3

  2 + 3

This is just syntax for adding two values together. You can imagine this as if
there was a built-in add function that takes two arguments. Luckily for us,
Python has a handful of operators available for all sorts of situations.

.. _operator: https://en.wikipedia.org/wiki/Operator_(computer_programming)


List of Basic Operators
=======================

Addition (+)
  Add two values together

  .. code-block:: pycon3

    >>> 2 + 2
    4
    >>> "Hello" + "World"
    "HelloWorld"

Subtraction (-)
  Subtract two values

  .. code-block:: pycon3

    >>> 2 - 2
    0
    >>> "Hello" - "World"
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    TypeError: unsupported operand type(s) for -: 'str' and 'str'

Multiplication (*)
  Multiply two value together

  .. code-block:: pycon3

    >>> 2 * 3
    6
    >>> "Sea shells by the sea shore " * 3
    "Sea shells by the sea shore Sea shells by the sea shore Sea shells by the sea shore"

Division (/)
  Divide two values

  .. code-block:: pycon3

    >>> 3 / 2
    1.5

  .. note::

    Division of numbers returns a ``float`` even if two numbers divide to a
    whole number.

Floor Division (//)
  Divide two values and truncate

  .. code-block:: pycon3

    >> 3 // 2
    1

Modulus (%)
  Calculate the remainder when dividing two numbers

  .. code-block:: pycon3

    >>> 3 % 2
    1

Exponent (**)
  Raise a value to a power

  .. code-block:: pycon3

    >>> 2 ** 3
    8

Equal To (==)
  Check if two types contain an equivalent value

  .. code-block:: pycon3

    >>> 1 == 1
    True
    >>> 0 == 1
    False
    >>> # They do not need to be the same type, just the same value
    ...
    >>> 1 == 1.000
    True

Not Equal To (!=)
  Check if two types contain a non-equivalent value

  .. code-block:: pycon3

    >>> 1 != 1
    False
    >>> 0 != 1
    True


Greater Than (>)
  Check if a type contains a value greater than another value

  .. code-block:: pycon3

    >>> 3 > 2
    True
    >>> 3 > 3
    False

Greater Than or Equal To (>=)
  Check if a type contains a value greater than another value, or if they two
  types are equal in value

  .. code-block:: pycon3

    >>> 3 >= 2
    True
    >>> 3 >= 3
    True

Less Than (<)
  Check if a type contains a value less than another value

  .. code-block:: pycon3

    >>> 2 < 3
    True
    >>> 3 < 3
    False

Less Than or Equal To (<=)
  Check if a type contains a value less than another value, or if they are
  equal in value

  .. code-block:: pycon3

    >>> 2 <= 3
    True
    >>> 3 <= 3
    True


Example
=======

The usage of Python's mathematical operators is pretty straight-forward since
they follow the order of operations. Unsurprisingly, parenthesis enforce the
order of operations.

.. code-block:: pycon3

  >>> (2 + 5) / 7
  1
  >>> (5 - 1) * ((7 + 1) / (3 - 1))
  16.0
