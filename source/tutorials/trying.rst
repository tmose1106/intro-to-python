*****************
Trying Things Out
*****************

Thus far, you have likely run into an error one way or another while learning
Python. As we know, when an error occurs, it will stop the program from
running, or even worse it may not even let it start. While we can not get
around problems with the code directly, like syntax errors, we can work around
almost any **`runtime error`_** using the ``try`` statement.

.. _`runtime error`: https://en.wikipedia.org/wiki/Run_time_(program_lifecycle_phase)

The Idea Behind ``try``
=======================

As you might guess, the ``try`` statement is used when we want to try
something. But what does that actually mean? Well, generally speaking it means
we would like to try some code that we know may cause an error, and if it does
we would like to handle it.

This new statement comes along with some new syntax:

* The word ``try`` followed by a colon, and then below is a block of code
  which may fail for a known reason.
* For each known error that may be **raised**, we use an ``except`` statement
  followed by the name of the error we wish to handle and a colon, then
  indented code for how we would like to handle the error.

.. code-block:: python3

  try:
      # Attempt to run some code
  except SomeError:
      # Run this code if SomeError occurs during the try block

As it turns out, handling an error is almost as easy as using ``if else``
statements.

An Example
==========

In several of the examples up to this point, we have asked the user to input
some type of information, and trusted that they would give us what we asked
for.
Let us say we have the following code:

.. literalinclude:: ../_static/examples/tutorials/trying/unsafe_prompt.py
  :language: python3
  :caption: unsafe_prompt.py
  :linenos:

The code above is short and concise. Unfortunately it has some problems: if the
user chooses to enter any number of other characters that do not constitute an
int or a float, the program will fail to due a **raised** error. Instead, we
can use the ``try`` and ``except`` statements like below:

.. literalinclude:: ../_static/examples/tutorials/trying/safe_prompt.py
  :language: python3
  :caption: safe_prompt.py
  :linenos:

While this code may be longer that the statement method, it also includes new
features:

* For starters, it is commented well
* It handles cases where the user enters an invalid form of input
* It is persistant, through the use of a ``while`` loop

Trying, No Matter What
======================

If you would like code to run no matter the result, you can add an optional
``finally`` statement.

.. code-block:: python3

  try:
      # Attempt to run some code
  except SomeError:
      # Run this code if SomeError occurs during the try block
  finally:
      # Always run this code, whether it fails or not

If the code were to simply follow the ``try`` and ``except`` blocks, it would
not run if an unknown error that wasn't caught had occured.

Common Errors
=============

Below are some common errors that might occur. A full `list of built-in
exceptions`_ can be found in the Python documentation.

ImportError
  When a module is requested but an error occurs during import

IndexError
  When an index is requested that does not exist

KeyError
  When a key is requested that does not exist

NameError
  When a variable is not found

FileNotFoundError
  When a file path is requested for opening that does not exist

.. _list of built-in exceptions: https://docs.python.org/3/library/exceptions.html
