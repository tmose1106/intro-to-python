*****************************
Controlling Flow of a Program
*****************************

In order to create *"smarter"* and more versatile programs, we must control
the *flow* of our programs. First we must explore available **logical
operations** which allow us to evaluate common conditions using boolean logic.
Next, we may study **conditional statements** which are essentially blocks of
code which can be ran at our discretion.

Logic Operators
===============

Python includes a set of logic operators which are incredibly readable and
allow us to compute logical operations using booleans.

This ``and`` That
-----------------

The ``and`` operator allows us to compute whether two values are ``True``.
In order for this operator to be ``True``, both conditions must be ``True``.

.. code-block:: pycon3

  >>> False and False
  False
  >>> False and True
  False
  >>> True and False
  False
  >>> True and True
  True

This ``or`` That
----------------

The ``or`` operator computes where one of the conditions is ``True``. In order
for this operator to be ``True``, at least one condition must be ``True``.

.. code-block:: pycon3

  >>> False or False
  False
  >>> False or True
  True
  >>> True or False
  True
  >>> True or True
  True

``not`` What You Think
----------------------

The ``not`` operator is used to negate or *"flip"* a boolean value. Odd
numbers of nots invert, even numbers of nots negate each other as shown below:

.. code-block:: pycon3

  >>> not True
  False
  >>> not False
  True
  >>> # Two nots cancel each other
  ...
  >>> not not True
  True

The Other ``or``
----------------

There are also many other operators out there. One of these is the **exclusive
or** AKA *xor*. This operator is true when the two conditions are not the
same, i.e. they can not both be ``True`` or ``False``. This operation can be
computed using the ``!=`` operator from before:

.. code-block:: pycon3

  >>> False != False
  False
  >>> True != False
  True
  >>> False != True
  True
  >>> True != True
  False

Note on Indentation
===================

In the following examples and many of the sections to follow we will be using
indentation. Each set of indentation denotes a `block`_ of code: A logical
grouping of lines of code. These blocks are typically denoted by a set of
curly braces in other programming languages. Python chooses to use this
indentation feature to enforce good code formatting and to denote where blocks
begin and end.

One must always remember the Python requires **exactly** four spaces to
represent a level of indentation. Most text editors will be aware of this, and
automatically place four spaces when you hit the ``Tab`` key. Some editors
will even recognize keywords allowing them to auto-indent as you type.
But it is important to pay attention to this, since an improperly indented
line of code can cause errors which are not always apparent at first glance.

.. _`block`: https://en.wikipedia.org/wiki/Block_(programming)

What ``if``?...
===============

The simplest conditional statement available in Python is the ``if`` statement.
The syntax is as follows:

* The word ``if``, followed by some type of condition, meaning some type with
  a value representing ``True`` or ``False``, or some operation which can be
  evaluated to such a value. The line must end with a colon.
* There must be an indented block of code below the ``if`` statement, which
  will be run *if* and *when* the condition on the inital line is considered
  ``True``.

Here is a simple example of how the ``if`` statement works:

.. literalinclude:: ../_static/examples/tutorials/conditionals/if_test.py
  :language: python3
  :caption: if_test.py
  :linenos:

::

  This will always run!

As we can see, the code under the if statement that has ``True`` as a
condition will run, but the if statement with ``False`` will not.

What ``else`` Can We Do?
========================

Using just ``if`` statements by themselves is not all that useful. Many times,
if some condition is not true, we would like to run a different piece of code.
For these cases, we can use the ``else`` statement.

.. literalinclude:: ../_static/examples/tutorials/conditionals/check_value.py
  :language: python3
  :caption: check_value.py
  :linenos:

::

  Please enter a number: 0
  Your value is zero.

When the condition of the ``if`` statement is not true, the indented code
below the ``else`` statement will be run.

If ``if`` and ``else`` Had a Kid
================================

There are also occasions when there are multiple cases that need to be checked
against. In this case, there is the ``elif`` statement. Essentially, it
functions identically to the ``if`` statement, but it must follow an ``if`` or
another ``elif`` statement.

.. literalinclude:: ../_static/examples/tutorials/conditionals/check_more_value.py
  :language: python3
  :caption: check_more_value.py
  :linenos:

::

  Please enter a number: -8
  Your value is less than zero.

One Line ``if``
===============

For when you only need a simple conditional statment, Python has a syntax
reminiscient of the basic if statement.

.. code-block:: pycon3

  >>> integer = 83
  >>> # We can resolve whether this number is even or odd on a single line
  ...
  >>> number_is_odd = True if integer % 2 else False
  >>> print(number_is_odd)
  True

.. note::

  If you are familiar with other programming languages, you might know this
  as the ``?:`` or `ternary operator`_.

.. _`ternary operator`: https://en.wikipedia.org/wiki/%3F:

Some Special Conditions
=======================

There a few keywords, special cases and style associated with conditional
statements.

Placement of Conditions
-----------------------

When creating a chain of ``elif`` statements, you may be wondering how each
case should be ordered. The general practice is to place the conditions which
will occur most often at the top of the chain.

Where's the Light Switch?
-------------------------

It may come as a suprise to those who have used other programming that Python
does not include a switch-case statement for situations where checking a
variable against several possible cases is necessary. The Python way of doing
this is simply chaining ``elif`` statements together.

The Pass Statement
------------------

A more obscure feature of Python is the inclusion of the ``pass`` statement.
It is used in cases where:

* The readability of the code can be increased
* A code block is necessary but the functionality is not

For example:

.. code-block:: python3

  # Enter into an if statement
  if True:
      # Do nothing, aka "pass"
      pass

An Odd (But Useful) Condition
-----------------------------

A frequently used conditional statement in Python is the
``if __name__ == "__main__"`` condition. Typically it appears at the bottom of
a file like so:

.. code-block:: python3

  if __name__ == "__main__":
      # Run some code...

This will begin to make more sense as we begin to `import modules`_ to
compartmentalize our code. The general rule-of-thumb is to use this
conditional when code is used for running quick tests of the file's contents.

.. _import modules: ./modules.html
