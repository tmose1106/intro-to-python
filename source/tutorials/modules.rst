******************************
Importing Modules and Packages
******************************

Now that we have discussed creating custom functions, dividing code into
reusable parts is possible. Unfortunately, this is not always enough; A program
with 100 functions inside of it is not very easy to read. Rather, Python
provides yet another tool for dividing code into pieces, called **modules**.

What is a Module?
=================

A module consists of a singular *.py* file containing Python code. These
modules can be **imported** into other Python modules and used there using
**dot notation**. Essentially, a module is a script of pre-existing code which
can be used within your own code. Many modules come with Python by default,
but many more exist online and just need to be installed.

Using Standard Modules
======================

The Python interpreter comes with a large list of modules which can be imported
and used in any Python script. This group of modules is called the **Standard
Library**. As with built-in functions, every version of Python includes the
same set of modules in its standard library. These modules are all documented
within in the `Official Python Documentation`_.

The easiest way to import a module is using the standard library. In usual
Python fashion, we can import a module using the ``import`` keyword, followed
by the name of the module we would like to import. By importing the module,
we can now use all of its contents using dot notation. Below is an example of
importing Python's standard ``math`` module, and using some of its included
functions and variables.

.. code-block:: pycon3

  >>> import math
  >>> # Calculate sine of zero
  ...
  >>> math.sin(0)
  0.0
  >>> # Print the value of pi
  ...
  >>> math.pi
  3.141592653589793
  >>> # Calculate sine of pi
  ...
  >>> math.sin(math.pi)
  1.2246467991473532e-16
  >>> # Calculating the cosine of pi
  ...
  >>> math.cos(math.pi)
  -1.0

There are several ways to learn what a Python module provides:

* By viewing the modules source code (the hardest)
* Using the built-in ``dir`` function on the module (quick, not really useful)
* Using the interpreter's built-in ``help`` function (quick, but not always
  useful)
* Using the module's documentation found online (slower and not always
  available, but likely the best if available)

Below is an example of the built-in ``dir`` method, which simple prints a list
of all available global variables, functions, and classes provided by the
standard ``math`` module. This list is not super helpful in that it does not
show which name is of which type.

.. code-block:: pycon3

  >>> dir(math)
  ['__doc__', '__file__', '__loader__', '__name__', '__package__', '__spec__',
  'acos', 'acosh', 'asin', 'asinh', 'atan', 'atan2', 'atanh', 'ceil',
  'copysign', 'cos', 'cosh', 'degrees', 'e', 'erf', 'erfc', 'exp', 'expm1',
  'fabs', 'factorial', 'floor', 'fmod', 'frexp', 'fsum', 'gamma', 'gcd',
  'hypot', 'inf', 'isclose', 'isfinite', 'isinf', 'isnan', 'ldexp', 'lgamma',
  'log', 'log10', 'log1p', 'log2', 'modf', 'nan', 'pi', 'pow', 'radians',
  'sin', 'sinh', 'sqrt', 'tan', 'tanh', 'tau', 'trunc']

Of course, the best way to see what the ``math`` modules has to offer is to
look at its `documentation`_ directly.

.. _`documentation`: https://docs.python.org/3/library/math.html
.. _`Official Python Documentation`: https://docs.python.org/3/library/index.html

Using Parts of Modules
======================

There is a special syntax for when one would like to use only a portion of
a module, and coincidentally not type as much. This syntax is as follows:

* The keyword ``from``, followed by the module name you would like to import
  from, followed by the ``import`` keyword, followed by a list of
  comma-separated variable/function/class names you would like to use.
* The selected items can now be used without using dot notation.

.. code-block:: pycon3

  >>> from math import cos, sin
  >>> sin(0)
  0.0
  >>> cos(0)
  1.0

Creating Simple Modules
=======================

A module is essentially a script without any top-level code. It mostly
includes definitions intended for use outside of the file itself.

Handling Top-Level Logic
------------------------

If module does have top-level logic for testing purposes, it is good practice
to place this code inside of an ``if __name__ == "__main__"`` statement. This
means the code will only be run if the file is explicitly ran on the command
line.

An Example
----------

Below are three example files:

* *int_stuff.py* and *user_input* are modules because they include no
  top-level code.
* *use_funcs.py* is a script (includes top-level code) which uses the two
  modules (using ``import`` statements).

.. literalinclude:: ../_static/examples/tutorials/modules/int_stuff.py
  :language: python3
  :caption: int_stuff.py
  :linenos:

.. literalinclude:: ../_static/examples/tutorials/modules/user_input.py
  :language: python3
  :caption: user_input.py
  :linenos:

.. literalinclude:: ../_static/examples/tutorials/modules/use_funcs.py
  :language: python3
  :caption: use_funcs.py
  :linenos:

::

  Please enter a list of integers separated by commas:
  2,25,-9,83
  The greatest integer is 83!

.. note::

  Notice how docstrings can be used to document modules by placing a multi-line
  string at the top of the file.

In Python we also have the luxury of including as many functions or any kind
of code you want in singular file. Instead of making two file, the above
example could just as easily included the two functions, or even just create
a script with no functions. By splitting our code into functions and modules,
it is easier for us to test and make changes to our programs.

What is a Package?
==================

A package is used to contain a project or a set of modules which can be
logically grouped together. A package can almost be thought of as a folder for
multiple modules. Interestingly enough, this is how we structure modules too.
The only caveat being we must include an **\_\_init\_\_.py** file in our
folder to tell Python that this is a package.

For example, to create a package we could use the following file structure:

::

    .
    └─ package_name/
       ├─ __init__.py   # File saying that this folder represents a package
       ├─ module_1.py   # Module with code inside
       └─ module_2.py   # Another module with code inside

Now that we have a package, we could do a few things. First, we could import
each module explicitly:

.. code-block:: python3

  import package_name # Imports the "package_name/__init__.py" file
  import package_name.module_1 # Imports the "package_name/module_1.py" file
  import package_name.module_2 # Imports the "package_name/module_2.py" file

Let's say there is a function defined in *package_name/module_2.py* called
``func()``. We could now call this function using
``package_name.module_2.func()``.

Alternatively, we could shorten the last two imports to:

.. code-block:: python3

  from package_name import module_1, module_2

Now we could call our ``func()`` function using ``module_2.func()``.

If we only wanted to use the ``func()`` function, we could get even more
explicit:

.. code-block:: python3

  from package_name.module_2 import func

And now we could simply use ``func()`` directly.

Using Packages
==============

We will be using the file structure described above for the examples below.

The contents of these files are:

.. literalinclude:: ../_static/examples/tutorials/modules/package_name/__init__.py
  :language: python3
  :caption: package_name/\_\_init\_\_.py
  :linenos:

.. literalinclude:: ../_static/examples/tutorials/modules/package_name/module_1.py
  :language: python3
  :caption: package_name/module_1.py
  :linenos:

.. literalinclude:: ../_static/examples/tutorials/modules/package_name/module_2.py
  :language: python3
  :caption: package_name/module_2.py
  :linenos:

We can directly import all of the modules one-by-one:

.. literalinclude:: ../_static/examples/tutorials/modules/use_package_name_1.py
  :language: python3
  :caption: use_package_name_1.py
  :linenos:

::

  Hi, it is me, package "package name" from __init__.py!
  Hi, it is me, "module_1"!
  Hi, it is me, "module_2"!

Alternatively, we can import the main module, and then use the ``from``
keyword to save a line and some typing of *package_name*:

.. literalinclude:: ../_static/examples/tutorials/modules/use_package_name_2.py
  :language: python3
  :caption: use_package_name_2.py
  :linenos:

::

  Hi, it is me, package "package name" from __init__.py!
  Hi, it is me, "module_1"!
  Hi, it is me, "module_2"!

We cannot be any more explicit here, since all of these functions have the same
name. If something is imported with an already existing name, it gets
overwritten. We can reproduce the output from before, but keep an eye on the
order of the imports.

.. literalinclude:: ../_static/examples/tutorials/modules/use_package_name_3.py
  :language: python3
  :caption: use_package_name_3.py
  :linenos:

::

  Hi, it is me, package "package name" from __init__.py!
  Hi, it is me, "module_1"!
  Hi, it is me, "module_2"!
