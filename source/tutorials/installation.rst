*****************
Installing Python
*****************

Python is available across several operating systems, including `Microsoft
Windows`_, `Apple OS X`_, `GNU/Linux`_ and `*BSD`_. Installing Python is
relatively easy across these operating systems.

.. note::

  At the time of writing, the latest Python version is **3.7.4**. I highly
  suggest installing the latest version if you can, but it is not mandatory
  for the sake of this tutorial. I only ask that you have some version of
  Python 3 installed.

.. note::

  If you are not interested in installing software to your computer,
  this syntax tutorial can be completed using a repl.it Python REPL. Look
  at the `Learning Python with repl.it`_ appendix if you are interested.

.. _`Apple OS X`: https://en.wikipedia.org/wiki/MacOS
.. _`*BSD`: https://en.wikipedia.org/wiki/Berkeley_Software_Distribution
.. _`GNU/Linux`: https://en.wikipedia.org/wiki/Linux
.. _`Microsoft Windows`: https://en.wikipedia.org/wiki/Microsoft_Windows
.. _`Learning Python with repl.it`: ../appendix/repl_it.html

Windows
=======

1. Visit the `Python download page`_
2. Click the yellow ``Download Python 3.7.4`` button
3. Run the downloaded *.exe* file
4. Complete the installation wizard


OSX
===

1. Visit the `Python download page`_
2. Click the yellow ``Download Python 3.7.4`` button
3. Run the downloaded *.dmg* file
4. Complete the installation wizard


GNU/Linux (Debian-based)
========================

1. Open a terminal
2. Run ``sudo apt-get install python3``

.. _`Python download page`: https://www.python.org/downloads/
