********************************
Iterating Over Items Using Loops
********************************

Looping can be used in coordination with our knowledge of containers to do
some serious work. Once data is placed within a container, we can look at each
item separately and apply logic to it. The practice of looping is often called
*iteration*.

Iterating ``while`` You Can
===========================

One of the simplest ways to loop is to set a variable, and decrement its value
using a ``while`` loop. As shown in the example below, a ``while`` loop will
continue repeating itself until the value it is provided becomes false. There
are a few things to note about the ``while`` loop:

* The loop requires a condition following the word *while*, which it will look
  at after each iteration. If the value is equal to ``True``, it will continue
* The condition **must** be followed by a colon
* The logic that the loop will run is *indented* below the loop declaration.
  All of the indented code will be run through until the condition becomes
  ``False``

.. code-block:: python3

  while some_condition:
      some_code()

Below is an example of how the ``while`` loop can be used to print out
numbers in descending order.

.. literalinclude:: ../_static/examples/tutorials/looping/while_countdown.py
  :language: python3
  :caption: while_countdown.py
  :linenos:

::

  5
  4
  3
  2
  1
  All done!

First, a variable *countdown_index* is created for storing the current value.
Next, we define the while loop and a condition for the loop to check on each
iteration. Since we are counting down to 0, we can use the fact that the
integer 0 is a falsy value to end the loop. For each iteration, we print the
current value and decrement the value. This loop will continue running these
indented lines of code (AKA a block) until the condition becomes false
(*countdown_index* equals 0). Once the loop ends, we print a message.

Iterating ``for`` Now
=====================

Python includes another type of loop known as a ``for`` loop. The ``for``
loop's job is to loop over a list of items. During each iteration, an item is
given a "temporary variable", per se.

* A ``for`` loop is started using the ``for`` keyword, followed by a
  "temporary variable" name, the ``in`` keyword, and then a container (or
  iterator) or a variable representing a container (or iterator).
* Each item in the list is iterated over once. During iteration, an item from
  within the container is set as the value for a "temporary variable" which
  can be used to access the current value. Once iteration is completed, the
  variable is automatically redefined.

.. literalinclude:: ../_static/examples/tutorials/looping/for_letters.py
  :language: python3
  :caption: for_letters.py
  :linenos:

::

  a
  b
  c
  d
  e

``for`` More Than One Variable
==============================

Occasionally, it may be useful to loop over two things at once. For this use
case, we use the built-in ``zip`` function. This essentially creates an
iterable similar to a list filled with tuples contain a value from each
argument's contents. We are able to loop over multiple items by creating
more *temporary variables* separated by commas.

.. literalinclude:: ../_static/examples/tutorials/looping/for_zip.py
  :language: python3
  :caption: for_zip.py
  :linenos:

::

  English: one
  Spanish: uno
  English: two
  Spanish: dos
  English: three
  Spanish: tres
  English: four
  Spanish: cuatro

Other times we may want to count how many values we have seen in a loop. The
``enumerate`` built-in function takes an iterable as an argument, and returns
a list of tuples containing an index integer and a value from the list.

.. literalinclude:: ../_static/examples/tutorials/looping/for_enum.py
  :language: python3
  :caption: for_enum.py
  :linenos:

::

  0 a
  1 b
  2 c
  3 d
  4 e

For or not to For?
==================

This loop is designed to serve the same exact function as the ``while`` loop
example above, but this is not always true. ``for`` and ``while`` loops are
designed with different purposes in mind.

For loops in Python are specially designed for when looping over values. If
the temporary value within a ``for`` loop is not being used, the standard is
to call the temporary value ``_``.

Meanwhile, ``while`` loops are better suited for situations where a loop
should run continuously, and the loop contains a point where the loop should
end. At this point, there isn't much we can do using ``while`` loops, but once
conditional statements are discussed, they will become quite powerful tools.

Modifying Loops
===============

We can also modify how loops will act using two keywords: ``break`` and
``continue``.

We use the ``break`` keyword whenever we would like to end a loop on-demand.
As soon as a ``break`` is reach, the loop will end, and whatever code that
follows the loop will proceed to run.

.. literalinclude:: ../_static/examples/tutorials/looping/simple_repl.py
  :language: python3
  :caption: simple_repl.py
  :linenos:

::

  Starting simple REPL
  >>> hi
  hi
  >>> skip
  >>> quit
  Stopping simple REPL

Working With Characters
=======================

At its lowest level, everything is a number to a computer. This fact includes
character symbols. Characters are defined through the American Standard Code
for Information Interchange (ASCII) standard. This standard defines which
numbers represent a symbol. For example, the number 97 is represented by the
letter "a" (specifically lowercase). The full table can be found on this
`Wikipedia page`_.

Luckily for us, Python includes built-in functions for converting characters
to their ASCII form and the other way around as well. To convert a letter to
its numerical ASCII value, we use the ``ord`` function. Likewise, to convert a
number to character, we use the ``chr`` function.

.. code-block:: pycon3

  >>> ord('a')
  97
  >>> chr(97)
  'a'

Now that we understand ASCII and how to work with it within Python, we can
use it to create an example tool. In this example, we will create a cypher for
hiding messages from "prying eyes" by creating an extremely basic cypher. To
do this, we will simply use what we know about strings, loops, and ASCII
characters. We can hide the meaning by "moving" the characters "over" so
to speak by adding one to all codes.

.. literalinclude:: ../_static/examples/tutorials/looping/ascii_cypher.py
  :language: python3
  :caption: ascii_cypher.py
  :linenos:

::

  Please enter a message: Hello there Alice!
  Ifmmp!uifsf!Bmjdf"

.. _`Wikipedia page`: https://en.wikipedia.org/wiki/ASCII#Printable_characters
