*************
Snazzy Syntax
*************

Python has some *seriously* snazzy syntax. It allows programmers to spend more
time thinking about the problem at hand, and less about little syntax
differences. And who doesn't like being more productive? So continue on with
the sections below and find out why Python is the `bee's knees`_!

.. toctree::
  :caption: Index
  :maxdepth: 1
  :numbered:

  intro
  installation
  interpreter
  comments
  types
  operators
  variables
  conditionals
  containers
  looping
  functions
  modules
  trying
  files
  classes
  object_orientation
  generators

.. _`bee's knees`: https://www.urbandictionary.com/define.php?term=bees%20knees
