*************************
Generators and One-Liners
*************************

Python has a special type of function known as a **generator**. Generators
allow us to return data, but rather than end the function it simply *"pauses"*
the function until it is asked for the next result. This makes our program
more efficient.

In more technical terms, we can create a generator function which returns an
object which can be iterated over, AKA an **interable**. To better understand
generators, we will first look at iterators and their counterparts. Then we
will examine custom generators.

A World Before Generators
=========================

In the past, when we needed to generate multiple items, we would create a
function that initializes a list. Afterwards, said list would be filled using
some type of loop and the list's ``append()`` method. Below is an example of
this for *generating* the first *n* perfect squares.

.. literalinclude:: ../_static/examples/tutorials/generators/perfect_square_list.py
  :language: python3
  :caption: perfect_square_list.py
  :linenos:

::

	[0, 1, 4, 9, 16]
	0
	1
	4
	9
	16

There are a few problems here:

* We are storing all the values in a list when we only need one at a time,
  taking up more space than necessary (i.e. we need 200+ perfect squares)
* We must wait for all the squares to be calculated before we can actually use
  them.

``yield`` Left
==============

To fix the above problems, we can use a **generator function** (or generator
for short). Creating a generator is as easy as creating an ordinary function.
The major difference is that we use the ``yield`` keyword rather than
``return``.

.. literalinclude:: ../_static/examples/tutorials/generators/perfect_square_generator_func.py
  :language: python3
  :caption: perfect_square_generator_func.py
  :linenos:

::

	<generator object perfect_squares at 0x7fedc52a9938>
	0
	1
	4
	9
	16

As shown in the above output, the generator function does not return a normal
container, but rather it returns a generator object. Python's ``for`` loop
understands how to use this object. At a high level, the following occurs:

* The ``for`` loop askes the generator for a result
* The generator computes a result, and yields a result, pausing the generator
  function
* The ``for`` loop uses the result as it's temporary variable and runs it's
  block
* The ``for`` loop askes the generator for another result
* The generator resumes until it yields another result

From here the cycle continues until the generator is finished.

One Line Generator
==================

Occasionally your generator is simple enough that you don't want to write an
entire function for it. In this case, Python provides the **generator
expression**, a one line generator. This is as simple as using a slight
modification of the familiar ``for`` loop syntax inside of parenthesis.

.. literalinclude:: ../_static/examples/tutorials/generators/perfect_square_generator_expr.py
  :language: python3
  :caption: perfect_square_generator_expr.py
  :linenos:

::

	<generator object <genexpr> at 0x7f8b1bbe9938>
	0
	1
	4
	9
	16

.. note::

	The value before the ``for`` keyword is what is yielded.

One-Line Lists and Dictionaries
=================================

Thus far, generators have given our old containers a bad wrap. But it is quite
the contrary: generators and lists can be used together to great effect.

Initially, we can pass a generator object to the built-in ``list()`` function:

.. code-block:: pycon3

	>>> list((number**2 for number in range(5)))
	[0, 1, 4, 9, 16]

This forces the generator to run until it is finished and fills a list.
Though, this operation is slightly ugly, so Python includes a simplified
syntax using square braces:

.. code-block:: pycon3

	>>> [number**2 for number in range(5)]
	[0, 1, 4, 9, 16]

Similarly, Python includes a syntax for creating one line dictionaries:

.. code-block:: pycon3

	>>> {number: number**2 for number in range(5)}
	{0: 0, 1: 1, 2: 4, 3: 9, 4: 16}

``yield from``
==============

There is an extension of the ``yield`` keyword called ``yield from``. This
essential allows us to create cascading generators AKA subgenerators which can
be used exactly like recursive functions. This behavior can be seen below in
our practical example.

A Practical Example
===================

For this example, we are going to create a simple clone of the `UNIX tree
command`_. Its use is displaying a nicely formatted text-based graphic of
a directory structure. Below is the output of the real ``tree`` command
showing the *examples* directory for this website.

::

	.
	├── classes
	│   ├── animal.py
	│   ├── imaginary.py
	│   ├── len_stupid.py
	│   └── simple_class.py
	├── conditionals
	│   ├── check_more_value.py
	│   ├── check_value.py
	│   └── if_test.py
	├── files
	│   ├── context_method.py
	│   ├── dumb_editor.py
	│   ├── path_exists.py
	│   ├── path_join.py
	│   └── unsafe_statement_method.py
	├── functions
	│   ├── arbitrary_args.py
	│   ├── find_max.py
	│   ├── greet.py
	│   ├── keyword_args.py
	│   └── shopping_list.py
	├── generators
	│   ├── directory_tree.py
	│   ├── fibonacci_generator.py
	│   ├── fibonacci_list.py
	│   ├── perfect_square_generator_expr.py
	│   ├── perfect_square_generator_func.py
	│   └── perfect_square_list.py
	├── looping
	│   ├── ascii_cypher.py
	│   ├── for_enum.py
	│   ├── for_zip.py
	│   └── while_print.py
	├── modules
	│   ├── int_stuff.py
	│   ├── package_name
	│   │   ├── __init__.py
	│   │   ├── module_1.py
	│   │   ├── module_2.py
	│   │   └── __pycache__
	│   │       ├── __init__.cpython-36.pyc
	│   │       ├── module_1.cpython-36.pyc
	│   │       └── module_2.cpython-36.pyc
	│   ├── __pycache__
	│   │   ├── int_stuff.cpython-36.pyc
	│   │   └── user_input.cpython-36.pyc
	│   ├── use_funcs.py
	│   ├── use_package_name_1.py
	│   ├── use_package_name_2.py
	│   └── user_input.py
	├── trying
	│   ├── get_input.py
	│   ├── safe_prompt.py
	│   └── unsafe_prompt.py
	└── variables
	    ├── get_name.py
	    └── variables_and_input.py

.. _`UNIX tree command`: https://en.wikipedia.org/wiki/Tree_(Unix)

Some ``os`` Functions
---------------------

For this program, we will want to use Python's built-in `os module`_.
Specifically, we will be using two functions:

``os.path.isdir()``
	Returns whether a path is a directory or not.

``os.scandir()``
	Returns an iterator of all items (file or directory) in a directory as
	``os.DirItem`` objects.

.. _`os module`: https://docs.python.org/3/library/os.html

The ``sys`` Module
------------------

We will also want to use the `sys module`_. This module includes features
which can allow us to create a friendly command line tool. Specifically, we
are going to use one list and one function:

``sys.argv``
	A list of additional command line arguments passed to the Python executable.

``sys.exit()``
	A function which ends the script immediately, taking an argument of the
	`return code`_.

.. _`return code`: https://en.wikipedia.org/wiki/Return_code
.. _`sys module`: https://docs.python.org/3/library/sys.html

Our Solution
------------

Now with this prerequisite knowledge, we can create a working solution.

.. literalinclude:: ../_static/examples/tutorials/generators/directory_tree.py
  :language: python3
  :caption: directory_tree.py
  :linenos:

The Result
----------

Now if we run this script in the *examples* directory:

::

	.
	├── conditionals
	│   ├── check_more_value.py
	│   ├── check_value.py
	│   ├── if_test.py
	├── functions
	│   ├── arbitrary_args.py
	│   ├── find_max.py
	│   ├── greet.py
	│   ├── keyword_args.py
	│   ├── shopping_list.py
	├── looping
	│   ├── ascii_cypher.py
	│   ├── for_enum.py
	│   ├── for_zip.py
	│   ├── while_print.py
	├── modules
	│   ├── user_input.py
	│   ├── int_stuff.py
	│   ├── use_funcs.py
	│   ├── __pycache__
	│   │   ├── user_input.cpython-36.pyc
	│   │   ├── int_stuff.cpython-36.pyc
	│   ├── package_name
	│   │   ├── __init__.py
	│   │   ├── module_1.py
	│   │   ├── module_2.py
	│   │   ├── __pycache__
	│   │   │   ├── __init__.cpython-36.pyc
	│   │   │   ├── module_1.cpython-36.pyc
	│   │   │   ├── module_2.cpython-36.pyc
	│   ├── use_package_name_2.py
	│   ├── use_package_name_1.py
	├── variables
	│   ├── variables_and_input.py
	│   ├── get_name.py
	├── files
	│   ├── context_method.py
	│   ├── path_exists.py
	│   ├── path_join.py
	│   ├── unsafe_statement_method.py
	│   ├── dumb_editor.py
	├── trying
	│   ├── get_input.py
	│   ├── unsafe_prompt.py
	│   ├── safe_prompt.py
	├── classes
	│   ├── animal.py
	│   ├── imaginary.py
	│   ├── len_stupid.py
	│   ├── simple_class.py
	├── generators
	│   ├── fibonacci_list.py
	│   ├── fibonacci_generator.py
	│   ├── perfect_square_list.py
	│   ├── perfect_square_generator_func.py
	│   ├── perfect_square_generator_expr.py
	│   ├── directory_tree.py

Of course, the output is not an exact match. Notice that:

* The last item in each directory doesn't have a angled corner
* The items are not sorted alphabetically

With enough work these features could be implimented, but that is beyond the
scope of this example.

Additional Resources
====================

* `python-course.eu: Generators`_
* `Understand generators in Python`_ on StackOverflow

.. _`python-course.eu: Generators`: https://www.python-course.eu/python3_generators.php
.. _`Understand generators in Python`: https://stackoverflow.com/questions/1756096/understanding-generators-in-python
