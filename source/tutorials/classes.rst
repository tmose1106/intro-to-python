*******************
Classifying Classes
*******************

Thus far we have learned a few ways to compartmentalize our code using
functions and modules. Now, we will be furthering our ability to
abstract code using **classes**.

Simply put, writing a `class`_ allows a programmer to create a *custom
type*. Recall from `Understanding Basic Types`_ that a type stores a value, and
that a value can be represented by more than one type. Perhaps less obvious
is that types allow for specific types of operations to be performed on their
contained values.

.. _class: https://en.wikipedia.org/wiki/Class_(computer_programming)
.. _Understanding Basic Types: ./types.html

Relation Between Object and Class
=================================

Before discussing classes, we must discuss **objects**. An object is
essentially an instance of a type. For example, we define a string object
like so:

.. code-block:: pycon3

  >>> my_string = "Hello, World!"

You might think this is just assigning to a variable, but what we have
actually done is initialize a ``str`` object with the value *"Hello, World!"*.
Behind the scenes there is code that allows us define a string object. This
code is called a **class**. If we check the type of the string we just created
we find a hint towards what our string actually is:

.. code-block:: pycon3

  >>> type(my_string)
  <class 'str'>

In order for a type to be create in Python, it must pass through two phases:

* A programmer must write a **class definition**, AKA a template for what
  properties an object will possess
* The **object**, which is an **instantiation** of the class and can be used
  by the programmer.

Some like to relate creating classes to baking cookies. The class definition
acts like the cookie cutter (defining the shape, size, etc. of the cookie),
while the object is akin to the cookie itself (the final product with all of
the expected properties). In other words, an object *is a thing* and a class
*defines that thing*.

.. image:: ../_static/images/gingerbread.jpg
  :alt: LEGO Ginerbread Man
  :align: center

Anatomy of a Class
==================

Now we will discuss the parts of a class definition. These parts can be broken
down into two main parts: *attributes* and *methods*.

**Attributes** are variables stored inside of a class. These variables act as
the data portion of the class; they define *the thing* our object represents.

**Methods** are functions that act upon the data stored within an object. They
are slightly different than functions we have dealt with so far, but will be
very much familiar.

.. note::

  Python does not allow for private attributes and methods. Instead,
  attributes and methods intended for internal use are named differently (as
  discussed below).

Defining a Class
================

There are a few syntax elements specific to a class:

* The ``class`` keyword, followed by the class name with *optional*
  parenthesis.
* An arbitrary amount of indented methods for the use within the class.

.. literalinclude:: ../_static/examples/tutorials/classes/simple_class.py
  :language: python3
  :caption: simple_class.py
  :linenos:

::

  Inital value: 3
  New value: 9

Above is a class called ``ClassName`` (which will also be the name of the
new type) containing two methods (called ``__init__`` and ``member_function``)
and a single attribute (called ``attribute``). Let's discuss these in depth:

* First is the method called ``__init__``. This function is a *magic method*
  (discussed later). All you need to know right now is that this particular
  method is ran when an object is created because of its intentional naming.
  It takes an argument (called ``value``) which is given during definition
  (see line 15 of the example).
* This method defines an **instance attribute**  because it is a value
  specific to this specific particular object.
* Finally, there is a method called ``member_function`` (another name for a
  method) which takes an argument (called ``multiplier``) and uses it to
  modify the value stored by ``attribute``.

Notice the use of the keyword ``self`` inside of this class definition. This
keyword refers to the object itself. In the example above,  we created an
object of type ``ClassName`` and stored it in ``my_object``. In this case, you
can essentially substitute the ``self`` variable with the ``my_object``
variable. All methods inside of a class must have their first argument
called ``self`` so that they may modify internal data.

.. note::

  ``self`` **must** be the first argument of a method!

The Style of Class Naming
=========================

There are several aspects for naming classes and their parts:

* Classes themselves are typically named with an uppercase letter at the
  beginning of each word in the name (i.e. ``ClassName``)
* Public attributes and methods are named the same as variables and functions
  with all lowercase and underscores where spaces would be (i.e. ``my_func``)
* Private attributes are named similarly to public attributes, but start with
  an underscore (i.e. ``_private_attribute``)

.. note::

  As always, these style conventions are not mandatory, but they are
  **highly** suggested.

Constructing a Class
====================

In order for classes to handle input at creation, there must be a
**constructor**. The constructor of a class is a method which take arguments
when the class is initialized and uses them to create the initial state for
the new object.

Let us look at the ``__init__`` method in the simple class from before:

.. literalinclude:: ../_static/examples/tutorials/classes/simple_class.py
  :language: python3
  :caption: simple_class.py
  :lines: 2-7
  :linenos:

Notice that the method takes two arguments: ``self`` and ``value``. As
discussed previously, all methods must have their first argument called
``self``. The second argument will be used by the method itself. In this case,
it is used to store ``value`` in ``attribute``. In reality this value could be
used for anything, just like any other variable or argument in a function.
But keep in mind, the point of the constructor is to set up the initial state
of the object. Nothing more, nothing less.

A Little Bit of Magic
=====================

Python's classes possess an interesting feature known as **magic methods**.
Magic methods allow classes to work seamlessly with the rest of the Python and
its built-in types and functions.

Fortunately, they are extremely easy to use. They are simply methods with
specific names defined by the language. All magic methods are all named with
double underscores on either side of the name (AKA **dunder methods** for
double underscore).

Take for example the ``str`` class. We can find the length of a string object
using the built-in ``len`` function like so:

.. code-block:: python3

  >>> my_string = "Hello there!"
  >>> len(my_string)
  12

How is this accomplished? How could Python possible interpreter the length of
some data stored within an object? The answer is using magic methods.

Python defines a special magic method called ``__len__`` which allows the
built-in ``len`` function to find the length of the string. So by defining
a ``__len__`` method on your own class with no arguments (except ``self``)
and returns an integer will make it possible.

.. literalinclude:: ../_static/examples/tutorials/classes/len_stupid.py
  :language: python3
  :caption: len_stupid.py
  :linenos:

Ta da, we have a function which more-or-less has the same functionality as the
built-in ``len`` function. Of course, Python includes many more ""*magic
methods*"" which can read about in the `Python Documentation`_.

.. _Python Documentation: https://docs.python.org/3/reference/datamodel.html#special-method-names

A Useful Example
================

A basic but more conventional use for a class could be defining imaginary
numbers. Here, magic methods are implemented for converting to a string and
adding two ImaginaryNumber objects.

.. literalinclude:: ../_static/examples/tutorials/classes/imaginary.py
  :language: python3
  :caption: imaginary.py
  :linenos:

::

  3+2i + 4-2i = 7.0∠0.0

Get, Set, Forget
================

When working with an object, we are manipulating data contained within said
object. For this reason, finding (*getting*) values and changing (*setting*)
values is an extremely common task.

Let's say we are modeling a clock using an object. In other programming
languages, we would write two methods to handle the hour value:

* ``getHour`` which returns the internal hour value
* ``setHour`` which checks whether the new value is valid and sets the internal
  value if it is valid

In Python, rather than forcing users to remember different methods for each
attribute, we use the ``property`` decorator on a method with the name of the
attribute we want users to work with. This decorator allows users to handle
our function as if they were using internal attributes directly.

Below is an example of a class definition for a 24-hour clock that uses
Python's ``property`` decorator along with a usage example:

.. literalinclude:: ../_static/examples/tutorials/classes/clock.py
  :language: python3
  :caption: clock.py
  :linenos:

::

  It is 01:00 right now
  Now it is 13:52
  Traceback (most recent call last):
    File "clock.py", line 42, in <module>
      my_clock.minute = 60  # This will raise an error
    File "clock.py", line 30, in minute
      raise ValueError("Invalid minute for 24-hour clock")
  ValueError: Invalid minute for 24-hour clock
