*************************
Understanding Basic Types
*************************

At its lowest level, computers understand data in terms of ones and zeros. But
these series of digits are meaningless without a corresponding `data type`_.
Types are defined differently by each programming language and provide an
outline for how data can be used.

For example, one can represent the number *83* in multiple ways:

.. code-block:: pycon3

  >>> type(83)
  <class 'int'>
  >>> type(83.0)
  <class 'float'>
  >>> type("83")
  <class 'str'>

Above, we use the built-in ``type()`` function to see what the type of a value
is. These are three different ways to represent the number *83*. It can be
represented by an integer value of *83*, a float with the value *83.0*, or a
string containing the characters *8* and *3*.

.. hint::

  You might notice that each type has the word *"class"* prepending it. This
  can be safely ignored until `Classes and Objects`_ are discussed later on.

.. _Classes and Objects: ./classes.html
.. _data type: https://en.wikipedia.org/wiki/Data_type

List of Basic Types
===================

Python provides several basic types:

``int``
  An integer representing a numerical value (i.e. 27)

``float``
  A decimal "floating point" number (i.e. 12.99)

``str``
  A list of characters in their "written" representation (i.e. "Hello, World!")

``bool``
  A value representing a true or false value (i.e. True or False)

``None``
  A value representing nothing (i.e. None)

.. note::

  The values ``True``, ``False``, and ``None`` must all be capitalized,
  otherwise Python will have no idea what you are trying to say and will
  provide a **syntax error**.

Converting Types
================

There are methods to convert some types to other types. For built-in types,
this is typically achieved using a respective built-in function with the same
name as the target type.

.. code-block:: pycon3

  >>> str(27)
  "27"
  >>> int("27")
  27
  >>> float(27)
  27.0
  >>> float("27")
  27.0
  >>> int("T")
  Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
  ValueError: invalid literal for int() with base 10: 'T'

If the value can not be converted, an error will be raised. As seen above,
the letter "T" can not be converted to an integer, so an error is raised.

.. important::

  If one were to run this code, it would end where the error was raised. This
  is due to an error occuring. As the **traceback message** in the example
  states, this is a **ValueError** due to the value *T* not having a
  base 10 equivalent.

  We will discuss errors and how to handle them in `Trying Things Out`_.

.. _Trying Things Out: ./trying.html

Truthy and Falsy Values
=======================

Interestingly but not particularly obvious is that some values are considered
"truthy" (equivalent to the boolean ``True``) and others are considered
"falsy" (equivalent to the boolean ``False``). A good way to learn which
values are which is using the built-in ``bool()`` function.

.. code-block:: pycon3

  >>> bool(0)
  False
  >>> bool(1)
  True
  >>> bool(-1)
  True
  >>> bool("")
  False
  >>> bool("content")
  True

This fact will come in handy when we learn about `conditional statements`_
later on.

.. _conditional statements: ./conditionals.html
