**********************
Commenting on Comments
**********************

An important part of programming is `commenting`_. A comment is a line of code
which is discarded during evaluation. Comments are intended to be read by
humans looking over code.

.. _`commenting`: https://en.wikipedia.org/wiki/Comment_(computer_programming)

Single Line Comments
====================

Single line comments begin with the ``#`` symbol and can be placed on their
own line.

.. code-block:: python3

	# Say hello to the world
	print("Hello, World!")
	# Say goodbye to the world
	print("Goodbye, World!")

Single line comments can also be placed at the end of a line of code using the
same syntax.

.. code-block:: python3

	print("Hello, World!") # Say hello to the world
	print("Goodbye, World!") # Say goodbye to the world

.. note::

	While it is not mandatory, it is good style to follow the comment symbol
	using a space.

Multi-line Comments
===================

Multiline comments can be created using triple quotation marks.

.. code-block:: python3

	"""
	Let's say hello to people because that is the kind and courteous
	thing to do.
	"""
	print("Hello, World!")


When to Comment
===============

Comments are an easy way to document your code. They have two side effects:

* Helping **others** understand your code more easily
* Helping **you** understand your code months later

This is why commenting (and documentation in general) is so important for a
program.

When Not to Comment
===================

It is also important to remember that too many comments is a problem. Code in
general should attempt to be **expressive** meaning that it is non-cryptic,
and can be understood by quick inspection. This can be attained by naming
things descriptively and keeping lines short. When you feel a line or block
of code is non-expressive, that is when you would prefer to comment.

Commenting on Comment Style
===========================

Some general rules of thumb for commenting style:

* Single line comments do not need punctuation. One the other hand, multiline
  comments should probably contain punctuation.
* Lines of code (in general) should try to be around 80 characters per line.
  For this reason, commenting on the end of a line is not always ideal.
* Multiline comments are general capped at 72 characters per line in Python.
