************
Introduction
************

About Programming (in General)
==============================

According to Wikipedia:

	Computer programming is a process that leads from an original formulation of
	a computing problem to executable computer programs. Programming involves
	activities such as analysis, developing understanding, generating algorithms,
	verification of requirements of algorithms including their correctness and
	resources consumption, and implementation of algorithms in a target
	programming language. Source code is written in one or more programming
	languages. The purpose of programming is to find a sequence of instructions
	that will automate performing a specific task or solving a given problem. The
	process of programming thus often requires expertise in many different
	subjects, including knowledge of the application domain, specialized
	algorithms, and formal logic.

At its simplest, `programming`_ is the act of taking logical reasoning,
defining it in a standardized format and passing it into a computer so that
a problem may be solved.

.. hint::

	The more professional term is "programming". The term "coding" is moreso an
	overused `buzzword`_.

.. _buzzword: https://en.wikipedia.org/wiki/Buzzword
.. _programming: https://en.wikipedia.org/wiki/Computer_programming

About Python
============

Here is a quote from Wikipedia:

  Python is an interpreted high-level programming language for general-purpose
  programming. Created by Guido van Rossum and first released in 1991, Python
  has a design philosophy that emphasizes code readability, and a syntax that
  allows programmers to express concepts in fewer lines of code, notably using
  significant whitespace. It provides constructs that enable clear programming
  on both small and large scales.

  Python features a dynamic type system and automatic memory management. It
  supports multiple programming paradigms, including object-oriented,
  imperative, functional and procedural, and has a large and comprehensive
  standard library.

  Python interpreters are available for many operating systems. CPython, the
  reference implementation of Python, is open source software and has a
  community-based development model, as do nearly all of its variant
  implementations. CPython is managed by the non-profit Python Software
  Foundation.

Below are a few key points. `Python`_ is...

* designed to be readable by anyone and allow easy expression of ideas
* much closer to written English and "batteries-included" than other languages
* tested by millions of individuals and gaining traction in enterprise
  environments
* cross-platforming allowing for programs to be reused on different systems
* absolutely free and supports your `computing freedom`_

.. _Python: https://en.wikipedia.org/wiki/Python_%28programming_language%29
.. _`computing freedom`: https://www.gnu.org/philosophy/free-sw.html

Where Python is Used
====================

Python is considered a "General-purpose" programming language, meaning it is
not designed for any specific real-world application but can be used for
(almost) any project.

Below is a list of fields where Python is commonly used along with some
popular tools and libraries which are used in said field (sorted
alphabetically).

* Mathematics/Data Science

	* `matplotlib`_, graphing capabilities similar to MATLAB
	* `numpy`_, scientific computing library with array support
	* `sympy`_, library for symbolic mathematics

* Machine/Deep Learning

	* `PyTorch`_, a deep learning framework for fast, flexible experimentation
	* `TensorFlow`_, an open source machine learning framework for everyone

* Web Developement

	* `Beautiful Soup`_, quick HTML parsing and scraping library
	* `django`_, framework for designing web applications
	* `pygments`_, code highlighting library
	* `requests`_, HTTP requests library

* Project Documentation

	* `docutils`_, reStructuredText markup parser
	* `Markdown`_, Markdown markup parser
	* `Sphinx`_, documentation generator

And there are many, many more tools and libraries available on PyPi_.

.. _`Beautiful Soup`: https://www.crummy.com/software/BeautifulSoup/
.. _`django`: https://www.djangoproject.com/
.. _`docutils`: http://docutils.sourceforge.net/
.. _`Markdown`: https://python-markdown.github.io/
.. _`matplotlib`: https://matplotlib.org/
.. _`numpy`: http://www.numpy.org/
.. _`pygments`: http://pygments.org/
.. _`PyPi`: https://pypi.python.org/
.. _`PyTorch`: https://pytorch.org/
.. _`requests`: http://docs.python-requests.org/en/master/
.. _`Sphinx`: http://www.sphinx-doc.org/en/stable/
.. _`sympy`: http://www.sympy.org/en/index.html
.. _`TensorFlow`: https://www.tensorflow.org/
