**********************
Working With Functions
**********************

When programming it is important to `not repeat previous work`_. Each time
work is repeated, it will become more work to refactor your program later on.
To prevent this problem, we want to design "pieces" of code which can be
reused at will. One such method is to create **functions**.

.. _not repeat previous work: https://en.wikipedia.org/wiki/Don%27t_repeat_yourself

Built-in Functions
==================

Throughout this tutorial there have been many mentions towards "built-in"
functions, but no real explaination as to what they are. In brief, built-in
functions are a set of functions that come "free of charge" when the Python
interpreter initializes. No matter how the script is run, as long as it is
using the same version of Python, there will always be the same set of
built-in functions available for use.

As of Python 3.6, there are 68 built-in functions available. Several of these
have already been mentioned, such as ``int``, ``input``, and ``type``. There
is a complete list of all built-in functions available in the `Python
Documentation`_.

.. _`Python Documentation`: https://docs.python.org/3/library/functions.html

Calling Functions
=================

The term *calling a function* simply means using a function. We do so by
typing the function name, then placing arguments separated by commas between
parenthesis.

.. code-block:: python3

  # Call a function called "function_name" with two arguments
  function_name(arg_1, arg_2)

Creating Custom Functions
=========================

As mentioned above, programs general consist of reusable pieces of code known
as functions. These are self-contained pieces of code which can be easily
created, used and even reused.

The syntax for functions is as follows:

* A function begins with the keyword ``def``, followed by a space, the
  function's name, an opening parenthesis, arguments separated by
  spaces and arguments (explained below), a closing parenthesis, and a colon
* Code for use within the function is indented below the function definition
* Functions are named the same way as variables (words separated by
  underscores)
* Arguments will be used inside of the function as **local variables**,
  meaning they can only be used in the function body.

.. code-block:: python3

  # Define a function
  def function_name(arg_1, arg_2):
      local_var = arg_1 + arg_2
      some_code()

  # Call the function defined above
  function_name(1, 2)

Returning Function Output
=========================

Functions would not be very useful if the data they process could not be used
externally. This is where the ``return`` keyword comes in. This keyword has
two purposes:

* The function ends when the first return statement is reached
* The data returned can be used outside the function body

Below is an example function which takes in a (preferably) numerical value
and checks its value relative to zero. When it is greater than zero, we return
a plus sign, less than a minus sign and if equal we return ``None``. Notice
that since we return for every possible case, the print statement never runs.

.. code-block:: pycon3

  >>> def true_or_false(value):
  ...     if value > 0:
  ...         return '+'
  ...     elif value < 0:
  ...         return '-'
  ...     else:
  ...         return
  ...     print("This won't ever print!")
  ...
  >>> true_or_false(3)
  '+'
  >>> true_or_false(-25)
  '-'
  >>> true_or_false(0)
  >>>

Below is an example of a function which finds the integer of greatest value
out of a list of integers. Once it has passed through all the values, it
returns the integer it found.

.. literalinclude:: ../_static/examples/tutorials/functions/find_max.py
  :language: python3
  :caption: find_max.py
  :linenos:

Unsurprisingly, Python already has a built-in function called ``max`` that
completes the same task. But nevertheless, we have created a simple
implimentation of that function which provides the same answer.

Documenting Functions
=====================

When we create functions, we would like to document them in orderly manner.
Luckily, Python provides a methods for doing so known as **docstrings**.
Essentially, a docstring is a multiline string placed at the very top of the
function body used to document the function itself.

Docstrings can be as many lines as necessary, but the standard is to have the
string beginning and ending on the same line if the docstring is short
(*a_func* below) or on their own lines if the docstring is long
(*another_func* below).

.. code-block:: python3

  def a_func():
      """ This is a short docstring. """
      pass

  def another_func():
      """
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
      eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
      enim ad minim veniam, quis nostrud exercitation ullamco laboris
      nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
      in reprehenderit in voluptate velit esse cillum dolore eu fugiat
      nulla pariatur. Excepteur sint occaecat cupidatat non proident,
      sunt in culpa qui officia deserunt mollit anim id est laborum.
      """
      pass

Types of Arguments
==================

A feature that sets Python apart from other programming languages is the
number of ways available to pass in arguments

Positional Arguments
--------------------

**Positional arguments** are defined by placing a name for the argument inside
of parenthesis in a function definition. This is the most common way to pass
in arguments.

Default Arguments
-----------------

When it comes to using functions, user (and programmers) do not want to specify
values for arguments every function call, but would like to be able to modify
how a function works. In this case, **default arguments** are extremely useful.
They have the following properties:

* Are created much like positional arguments, but are followed by an equals
  sign (with no surrounding space), and then a default value
* Can be modified in the same way they are called (name, equals, then value)
* Can be passed with the name of the argument and out of order, or without
  the name but in order of how they are defined

Here is an example of a function which greets someone like before, but the
number of times and the greeting string can be changed.

.. literalinclude:: ../_static/examples/tutorials/functions/greet.py
  :language: python3
  :caption: greet.py
  :linenos:

::

  Trial #1
  Hello, World!

  Trial #2
  Goodybe, Cruel World!

  Trial #3
  Goodbye, Cruel World!

  Trial #4
  Greetings, Alice!
  Greetings, Alice!
  Greetings, Alice!

.. note::

  Notice how the formatted string is created *before* looping begins.
  Alternatively, the program could create the formatted string within the loop,
  but the downside is that it would need to be recreated for each iteration of
  the loop; This is **not** a good behavior.

Look at how many uses our three line function now has! Alternatively, each
statement could be printed one-by-one in a script, but using functions
decreases the amount of code that needs to be changed when an edit is
necessary.

Arbitrary Arguments
-------------------

Sometimes a function may not be able to encompass every single use case, so the
programmer may wish to have a variable number of arguments. These come in two
forms: **arbitrary arguments** and **keyword arguments**.

Arbitrary arguments are a list of arguments which can be provided an arbitrary
number of times, typically used at the end of an argument list. The user can
add as many arguments as they like and the function will handle them. For
example, one could create the following function.

.. literalinclude:: ../_static/examples/tutorials/functions/arbitrary_args.py
  :language: python3
  :caption: arbitrary_args.py
  :linenos:

::

  Hello, Alice!
  Hello, Bob!
  Hello, Cármèn!

  Hi, Alice!
  Hi, Bob!
  Hi, Cármèn!


Notice how there is an asterisk before the argument ``names``; This specifies
that this argument is a positional argument. A positional argument is given
back to the function as a tuple containing all of the values passed.

Keyword Arguments
-----------------

Keyword arguments are very much a combination of the syntax of specifying
default arguments with the varability of arbitrary arguments. Instead of
giving the function a list of values, it is provided with a dictionary.

.. literalinclude:: ../_static/examples/tutorials/functions/keyword_args.py
  :language: python3
  :caption: keyword_args.py
  :linenos:

::

  Greetings, Alice!
  Hi, Bob!
  Hoi, Cármèn!

  Greetings, Alice!
  Hi, Bob!
  Hoi, Cármèn!

An Example
==========

.. note::

  Before beginning this example, you may want to check out `Stringing Together
  Strings`_, specifically the section on string formatting.

In this list, we are going to create a function which allows one to enter a
shopping list and print it out in a formatted fashion.

.. literalinclude:: ../_static/examples/tutorials/functions/shopping_list.py
  :language: python3
  :caption: shopping_list.py
  :linenos:

::

  Details:
  --------
    + Store: ShopRite
    + Day: Monday
    + Time: 9:00 P.M.

  Items:
  ------
    1. Bread [ ]
    2. Milk [ ]
    3. Eggs [ ]

.. _`Stringing Together Strings`: ../appendix/strings.html

Recursion
=========

Occasionally it is helpful to large problems and break them into smaller
parts. One such case is computing the `factorial`_ of a number. The factorial
of *5* is equal to *5 \* 4 \* 3 \* 2 \* 1*. We can look at this problem more
explicitly with parenthesis, leading to *5 \* (4 \* (3 \* (2 \* (1))))*.

Pragmatically, we can make a function that does the same thing using
`recursion`_. In terms of programming, recursion refers to a function calling
itself and waiting for a result.

.. literalinclude:: ../_static/examples/tutorials/functions/factorial.py
  :language: python3
  :caption: factorial.py
  :linenos:

::

  0! = 1
  1! = 1
  2! = 2
  3! = 6
  4! = 24
  5! = 120
  6! = 720
  7! = 5040
  8! = 40320
  9! = 362880
  10! = 3628800
  11! = 39916800
  12! = 479001600
  13! = 6227020800
  14! = 87178291200
  15! = 1307674368000
  16! = 20922789888000
  17! = 355687428096000
  18! = 6402373705728000
  19! = 121645100408832000

Let's take the factorial of *3* as an example. The first time the function
is called with an argument of *3*. This is not *1* or *0*, so we want to
return *3* times the factorial of *2*. But before this can be returned, the
factorial of *2* must be computed. The function is ran again with an argument
of *2*. Again, this is not *1* or *0*, so we want to return *2* times the
factorial of *1*. The function is called a third time with an argument of *1*.
This time *1* is *1* or *0*, so we return *1*. Now the factorial of *2* can be
resolved, and then the factorial of *3* can be resolved.

While this may seem a bit convoluted at first, we have now made a function
which can solve hundred of factorials in only 5 lines of code!

.. image:: ../_static/images/python_meme.jpg
  :alt: Python Meme
  :height: 300px
  :align: center

.. note::

  This function **cannot** solve for an infinite number of factorials since
  Python does not allow there to be more the 1000 levels of recursion at once
  and will raise a ``RuntimeError`` if attempted.

.. hint::

  Don't be scared by this example, recursion is not that common in Python.


.. _`factorial`: https://en.wikipedia.org/wiki/Factorial
.. _`recursion`: https://en.wikipedia.org/wiki/Recursion_(computer_science)
