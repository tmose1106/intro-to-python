**************
Creating Files
**************

A common practice for programs is to open and write data to files. Files are
how we store data that remains persistent even when we turn off the computer.

When it comes to working with files, there are several possible operations
which may need to be performed. Most of these operations attempt to prevent
a runtime error related to the file from occuring.

Forunately, Python includes cross-platform tools for doing so. Most of these
tools are included in the ``os`` and ``os.path`` modules.

When it comes to working with files, programs require a **path**. As
mentioned in the `Command Line Tutorial`_, a path is an operating system
dependant way of representing a file or directory. If we would like to access
a file, we must know where the file's path is.

Verifying Existence
===================

An important task when working with files is verifying their existence. If one
attempts to open a file that does not exist, there will be a
``FileNotFoundError``. Many times we would like to know if a file or directory
exists, and then act accordingly if it doesn't.

First and foremost, we need to create cross platform paths. This can be
achieved using the ``join`` function in the ``os.path`` module. This will join
together multiple "parts" of a path passed as arbitrary arguments.

.. literalinclude:: ../_static/examples/tutorials/files/path_join.py
  :language: python3
  :caption: path_join.py
  :linenos:

The output of this script will appear differently depending on the current
operating system. On Windows, the output will be:

::

  Documents\Python

And on UNIX-like system is will appear as:

::

  Documents/Python

Now that path strings can be generated, we should check whether the path we
want exists. For this we can use an assortment of functions:

``exists()``
  Check if a path exists

``isdir()``
  Check if a path defines a directory

``isfile()``
  Check if a path defines a file

``islink()``
  Check if a path defines a shortcut file

Using these, we can create a script that checks what type of file a sample
file called *sample.txt* is:

.. literalinclude:: ../_static/examples/tutorials/files/path_exists.py
  :language: python3
  :caption: path_exists.py
  :linenos:

Opening a File
==============

The way we access files is using the built-in ``open`` function. As mentioned
above we only need a path, which in this case will be placed inside of a
string. In the example below we are on a Windows-based computer, and we want
to open a file in our current directory called ``sample.txt``. We are then
going to try reading the contents of the file and print them out.

.. code-block:: pycon3

  >>> # Open the file for reading
  ...
  >>> our_file = open(".\\sample.txt")
  >>> # Read the file contents into a string, and then print the string
  ...
  >>> print(our_file.read())
  >>> # We should close the file when we are done with it
  ...
  >>> our_file.close()

Now that we know how to open a file for reading, you might wonder how we write
to a file. There are several modes for opening a file, each with its own use
case. How we choose to open the file depends on what we would like to do with
it. In the example below, we pass another argument to the ``open`` function,
which defines how we would like to open the file.

In this example, we are opening a file for writing. To do so we pass another
argument to the ``open`` function to tell Python that we would like to write
to the file.

.. code-block:: pycon3

  >>> # Again, open the file, but this time for writing
  ...
  >>> our_file = open(".\\sample.txt", 'w')
  # Write new contents to the file
  ...
  >>> our_file.write("Hello, World!")
  # Again, we should close the file
  ...
  >>> our_file.close()

.. note::

  In this example we only ask for write access using the ``'w'`` as an
  argument. If one attempts to use the ``read`` method on the file object,
  it will raise an *io.UnsupportedOperation* error since read access is not
  provided.

More information about the ``open`` function can be found in the
`Official Python Documentation`_.

.. _`Command Line Tutorial`: ../appendix/command_line.html
.. _`Official Python Documentation`: https://docs.python.org/3/library/functions.html#open


Opening with ``with``
=====================

Yet another obscure Python statement is the ``with`` statement, which is also
known as a **context manager**. These context managers are designed for
situations where no matter what the code does (passes, fails, etc.), the
context manager will always run some code at the opening and closing of the
code block. The syntax is as follows:

* The ``with`` keyword followed by an object which supports context managers,
  followed by the ``as`` keyword, a temporary variable name, and then a colon.
* As with the most other keywords, below the statement there is indented code.
  This code is the "context" where our temporary variable is available.

The most prominent example of context managers is for opening files. Below is
an example using the statement method from before, followed by the more
*Pythonic* "context manager" method:

.. literalinclude:: ../_static/examples/tutorials/files/unsafe_statement_method.py
  :language: python3
  :caption: unsafe_statement_method.py
  :linenos:

.. literalinclude:: ../_static/examples/tutorials/files/context_method.py
  :language: python3
  :caption: context_method.py
  :linenos:

These two code blocks complete the same task of printing a file's contents.
The context manager knows that when the indented code below it ends the file
should be closed. Any operations on the file's contents should be completed in
this indented code. This makes it more clear what is happening in your code.

An Example
==========

We now have enough knowledge to create an extremely simple text editor. The
editor will accept user input until the user enters a line reading *"EOF"*,
indicating the End Of the File. Each line accepted will be written to a
text file called *editor.txt*.

.. literalinclude:: ../_static/examples/tutorials/files/dumb_editor.py
  :language: python3
  :caption: dumb_editor.py
  :linenos:

Now if the user types the following ("⏎" representing pressing Enter):

::

  Hello⏎
  There⏎
  Alice⏎
  EOF⏎

The text file *editor.txt* will contain all of the text preceeding the line
containing *"EOF"*:

::

  Hello
  There
  Alice
