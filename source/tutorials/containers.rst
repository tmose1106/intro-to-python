*******************
Containers for Data
*******************

One of the major reasons to use Python is its tight integration with built-in
containers. Out of the box, Python has a few different containers, each with
their own special use case. Together, these containers can be used to create
extremely expressive data types.

Lists
=====

A ``list`` is very straight-forward in that its job is to keep a list of
ordered items. A list has the following properties:

* Are created using square braces (``[`` and ``]``) and items separated by
  comments
* Can contain any type (including other lists)
* Retain the original order of it's items
* Allow for indexing of items
* Can be easily extended and shortened

First, we can define a list like below:

.. code-block:: pycon3

  >>> my_list = ['a', 'b', 'c', 'd', 'e']

Next, we can pick items out of a list using **indexing**:

.. code-block:: pycon3

  >>> my_list[0]
  'a'
  >>> my_list[3]
  'd'
  >>> # Cause an error by trying to choose an index that does not exist
  ...
  >>> my_list[5]
  Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
  IndexError: list index out of range

.. note::

  It is important to notice that strings and many other container types
  utilize this same indexing behavior.

We can also select multiple a subset of the list using indexing:

.. code-block:: pycon3

  >>> my_list[1:4]
  ['b', 'c', 'd']
  >>> my_list[1:-1]
  ['b', 'c', 'd']
  >>> my_list[2:]
  ['c', 'd', 'e']

Items can be easily added to the end of a list using the ``append``
**method**:

.. code-block:: pycon3

  >>> my_list.append('z')
  >>> my_list
  ['a', 'b', 'c', 'd', 'e', 'z']

.. note::

  You may not understand what a method is at this moment. Much like functions,
  they will be discussed in-depth later on in this tutorial. For the moment,
  just know they are functions that act upon a type's internal value.

Values at an index can be edited using a combination of indexing and
assignment:

.. code-block:: pycon3

  >>> my_list[2] = 'z'
  >>> my_list
  ['a', 'b', 'z', 'd', 'e', 'z']

The first occurrence of an item can be removed using the ``remove`` method:

.. code-block:: pycon3

  >>> my_list.remove('z')
  >>> my_list
  ['a', 'b', 'd', 'e', 'z']

The last item of a list can be removed using the ``pop`` method:

.. code-block:: pycon3

  >>> my_list.pop()
  'z'
  >>> my_list
  ['a', 'b', 'd', 'e']

An item can be removed by index using the ``del`` statement (short for
*delete*):

.. code-block:: pycon3

  >>> del my_list[0]
  >>> my_list
  ['b', 'd', 'e']

To check if a value is inside of a list, use the ``in`` keyword:

.. code-block:: pycon3

  >>> my_list
  ['b', 'd', 'e']
  >>> 'b' in my_list
  True
  >>> 'z' in my_list
  False

We can determine the number of items in a list (and many other Python types)
using the built-in ``len()`` function (short for *length*).

.. code-block:: pycon3

  >>> len(my_list)
  4

Tuples
======

There is another container type known as a ``tuple`` which is `immutable`_,
meaning values can not be changed once the container is created. Otherwise,
they act exactly like lists while indexing.

.. code-block:: pycon3

  >>> my_tuple = (1, 2, 3)
  >>> my_tuple[1]
  2
  >>> my_tuple[1] = 4
  Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
  TypeError: 'tuple' object does not support item assignment

.. _`immutable`: https://en.wikipedia.org/wiki/Immutable_object

Tuples also include the ability to *unpack* contained values to individual
variables using syntax similar to multiple assignment:

.. code-block:: pycon3

  >>> x, y, z = (1, 2, 3)
  >>> print(x)
  1
  >>> print(y)
  2
  >>> print(z)
  3
  >>> a, b, c = ("Hello", "World")
  Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
  ValueError: not enough values to unpack (expected 3, got 2)

Notice how unpacking does not work unless there are an equal number of
variables and values.

Sets
====

Another Python container type is the ``set``. A set is a group of items which
cannot contain the same item twice. This is done automatically by the set
itself.

* Created using curly braces (``{`` and ``}``) and comma separated values
* Items cannot be repeated (handled automatically)
* Items can be added and removed

.. code-block:: pycon3

  >>> my_set = {1, 2, 3, 4, 3, 2, 1}
  >>> my_set
  {1, 2, 3, 4}
  >>> my_set.add(5)
  >>> my_set
  {1, 2, 3, 4, 5}


Dictionaries
============

Last (but not least) is Python's ``dictionary`` container. This type is used
for mapping one value to another.

* Dictionaries are created using curly braces (``{`` and ``}``) followed by a
  key (typically a string), a colon, a space, a value, and then a comma
* The name which represents a value is called a *key*
* Values can be of any type, while keys can not

Dictionaries can be create on a single line:

.. code-block:: pycon3

  >>> my_dictionary = {"one": "uno", "two": "dos", "three": "tres"}

Or they can be create on multiple lines for readability:

.. code-block:: pycon3

  >>> my_dictionary = {
  ...     "one": "uno",
  ...     "two": "dos",
  ...     "three": "tres",
  ... }

An item can be selected using its key:

.. code-block:: pycon3

  >>> my_dictionary["one"]
  'uno'
  >>> my_dictionary["quince"]
  Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
  KeyError: 'quince'

Items can be added using key indexing and assignment:

.. code-block:: pycon3

  >>> my_dictionary["four"] = "cuatro"

An iterable of the keys and values can be created using the ``keys`` and
``values`` methods:

.. code-block:: pycon3

  >>> my_dictionary.keys()
  dict_keys(['one', 'two', 'three', 'four'])
  >>> my_dictionary.values()
  dict_values(['uno', 'dos', 'tres', 'cuatro'])

.. note::

  The iterables returned are *dict_keys* and *dict_values* objects,
  respectively. These objects are only used for iteration (which will be
  discussed in the next tutorial). If you wish to modify the values, they
  must be converted to a *list* first using the built-in ``list`` function.

An iterable of all the items in a dictionary can be generated using the
``items`` method:

.. code-block:: pycon3

  >>> my_dictionary.items()
  dict_items([('one', 'uno'), ('two', 'dos'), ('three', 'tres'), ('four', 'cuatro')])

Much like with lists, the ``len()`` function can be used to determine the
number of items in a dictionary:

.. code-block:: pycon3

  >>> len(my_dictionary)
  4

Finally, items can be removed from a dictionary using the ``del`` statement:

.. code-block:: pycon3

  >>> del my_dictionary["two"]
  >>> my_dictionary
  {'one': 'uno', 'three': 'tres', 'four': 'cuatro'}
