*******************
Assigning Variables
*******************

Assigning variables in Python is extremely easy. In order to do so, one should
use the syntax shown below:

.. code-block:: pycon3

  >>> # This is how you assign a variable
  >>> my_greeting = "Greetings!"
  >>> print(my_greeting)
  Greetings!

As you can see:

* The characters to the left of the equals sign are the name of your variable
* Whatever is on the right of the equals sign is what we would like the
  variable to represent.
* The single equals sign (surround by a single space on either side) tells
  Python we would like to assign the value on the right to the variable name on
  the left.

.. note::

  When using Python, there is an accepted standard for styling our code. In
  this example, we see the style for variable names: lowercase words separated
  by underscores.

In this case, we are assigning a string to a variable. We do not need to tell
Python the type for the variable as you may in other programming languages.
This style of defining variables is known as *dynamic typing*. Another
side-effect of dynamic typing is we can reassign variables the same way that we
assign them.

.. code-block:: pycon3

  >>> # We will assign a string to a variable
  ...
  >>> name = "Alice"
  >>> print(name)
  Alice
  >>> # Now we will reassign the variable
  ...
  >>> name = "Bob"
  >>> print(name)
  Bob
  >>> # Now we will reassign to a different type
  ...
  >>> name = 83
  >>> print(name)
  83

Naming Variables
================

When it comes to naming variables in Python, there are a few things to
remember. Variable names...

* can contain lowercase letters, uppercase letters, numbers and underscores
* may not contain spaces (use underscores instead!)
* may not start with a number
* *should* be descriptive of what the programmer is trying to do with said
  variable

.. code-block:: pycon3

  >>> this_works = 23
  >>> this_works_2 = 45
  >>> 3rd_times_the_charm = 125
    File "<stdin>", line 1
      3rd_times_the_charm = 125
                        ^
  SyntaxError: invalid syntax
  >>> no-way = 35
    File "<stdin>", line 1
  SyntaxError: can't assign to operator

.. note::

  Above is your first look at an error in Python. We will discuss what errors
  are and how work around some of them in the `Trying Things Out tutorial`_.

.. _Trying Things Out tutorial: ./trying.html

Assignment Operators
====================

Python has a neat syntax for performing an operation on an assigned value
and reassigning the value in a single operation. In other languages, this
is similar to the incriment and decrement operators.

.. code-block:: pycon3

  >> x = 1
  >> x += 1
  >> x
  2

These operators also exist for subtraction, multiplication, division,
floor division and exponential operators.

.. note::

  There is no `++` or `--` operator in Python, for the sake of simplicity.

Multiple Assignments
====================

Another interesting feature is assigning multiple variables on a singular
line. This can be accomplished using commas to separate the variable names
and associated values.

.. code-block:: pycon3

  >>> x, y, z = 1, 2, 3
  >>> x
  1
  >>> y
  2
  >>> z
  3

Communicating With the User
===========================

Programs typically act upon some kind of input from a user. The simplest way
to get user input in Python is the built-in ``input`` function. It takes one
argument: a string to prompt the user.

.. literalinclude:: ../_static/examples/tutorials/variables/get_name.py
  :language: python3
  :caption: get_name.py
  :linenos:

::

  What is your name?: Alice
  Hello Alice

.. note::

  Code examples with a file name caption can be found in the `Examples
  Repository`_.

.. _Examples Repository: https://gitlab.com/tmose1106/intro-to-python-examples

Putting It All Together
=======================

Now that we know all of the basic operators, we can use this knowledge along
with our new-found knowledge of types and operators to create our first real
program!

In the example below, you will see the built-in ``input`` function,
which gathers text input from the user. While you may not understand the usage
of this function at the moment, all you need to know right now is that the
``input`` function returns a string.

.. literalinclude:: ../_static/examples/tutorials/variables/variables_and_input.py
  :language: python3
  :caption: variables_and_input.py
  :linenos:

::

  What is your age?: 23
  You are over 276 months young!
