*******
Level 1
*******

Hear My Angle (Become WEBSCALE)
===============================

Problem
-------

A common activity on the subreddit `/r/programmingcirclejerk`_ is to
"angle post" AKA "webscale post": write a string horizontally and vertically
with each character separated by a space. Write a script that takes in a
string from the user and makes it "webscale".

.. _`/r/programmingcirclejerk`: https://old.reddit.com/r/programmingcirclejerk/

Sample Output
-------------

::

  Please input a string: hello
  h e l l o

  e

  l

  l

  o

::

  Please input a string: RATHER ARCHAIC MINDSET
  R A T H E R   A R C H A I C   M I N D S E T

  A

  T

  H

  E

  R



  A

  R

  C

  H

  A

  I

  C



  M

  I

  N

  D

  S

  E

  T

Turning Up the Heat
===================

There are three common temperature scales: Fahrenheit (F), Celcius (C) and
Kelvin (K). Create a script that takes in a temperature reading as input
(i.e ``32.0F``, ``0.0C``, ``273.15K``) and prints this reading in all three major
scales.

Hints
-----

You may be interested in avoiding precision errors when printing results. This
can be mostly avoided using the built-in ``round()`` function.

Sample Output
-------------

::

  Please enter a temperature reading: 0C
  Celcius: 0.0
  Fahrenheit: 32.0
  Kelvin: 273.15

::

  Please enter a temperature reading: 32F
  Celcius: 0.0
  Fahrenheit: 32.0
  Kelvin: 273.15

::

  Please enter a temperature reading: 273.15K
  Celcius: 0.0
  Fahrenheit: 32.0
  Kelvin: 273.15

::

  Please enter a temperature reading: 491.67R
  Invalid unit provided: R
