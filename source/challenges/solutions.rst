*********
Solutions
*********

Below are general solutions for all of the problems, sorted alphabetically.

.. toctree::
  :caption: Index
  :maxdepth: 1

Hear My Angle
=============

.. literalinclude:: ../_static/examples/challenges/hear_my_angle.py
  :language: python3
  :caption: hear_my_angle.py
  :linenos:

Making Change
=============

.. literalinclude:: ../_static/examples/challenges/making_change.py
  :language: python3
  :caption: making_change.py
  :linenos:

String Reversal
===============

.. literalinclude:: ../_static/examples/challenges/string_reversal.py
  :language: python3
  :caption: string_reversal.py
  :linenos:

To Pig Land
===========

.. literalinclude:: ../_static/examples/challenges/to_pig_land.py
  :language: python3
  :caption: to_pig_land.py
  :linenos:

Treasure Hunt
=============

.. literalinclude:: ../_static/examples/challenges/treasure_hunt.py
  :language: python3
  :caption: treasure_hunt.py
  :linenos:

Turning Up the Heat
===================

.. literalinclude:: ../_static/examples/challenges/turning_up_the_heat.py
  :language: python3
  :caption: turning_up_the_heat.py
  :linenos:
