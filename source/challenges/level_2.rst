*******
Level 2
*******

To Pig Land
===========

Prerequisites
-------------

* You will need to know about functions
  * The syntax for defining of a function
  * How to return a value
* It would be helpful to understand the the string methods ``join()`` and
  ``split()``.

Problem
-------

We want a function that converts a string containing words (not necessarily a
complete sentence) and converts each of them to Pig Latin.

.. note::

  One might think of extending this by creating a reverse Pig Latin converter,
  but unfortunately Pig Latin is not the best thought-out language and there
  is no way to reverse a number of words.

Hints
-----

* You may assume that the input string is all lowercase, and all words are
  separated by spaces
* You can check the validity of your custom messages using the unofficial
  reference, `piglatin.org`_

.. _`piglatin.org`: http://www.piglatin.org/

Solution Output
---------------

::

  Before Pig Latin: the quick brown fox
  After Pig Latin: ethay uickqay ownbray oxfay

::

  Before Pig Latin: lorem ipsum dolor sit amet
  After Pig Latin: oremlay ipsumway olorday itsay ametway

::

  Before Pig Latin: hello there alice
  After Pig Latin: ellohay erethay aliceway

::

  Before Pig Latin: no way jose
  After Pig Latin: onay ayway osejay

Strings Reversal
================

Problem
-------

We want to reverse a user input string in a special way. We want to print a
string, but with each iteration we want to cut off a character from the
beginning and add the next ending character. For example, if the user enters
*"hello"*, during the next iteration we want to cut off the *"h"* and add an
*"l"* to the end resulting in the string *"ellol"* (see the Solution Output
below).

.. note::

  This problem was inspired by `this Reddit comment`_ on
  /r/programmingcirclejerk.

.. _`this Reddit comment`: https://old.reddit.com/r/programmingcirclejerk/comments/8r24d1/why_are_you_complaining_about_memory_usage_most/e0oggzb/

Hints
-----

My solution to this problem makes extensive use of indexing. You may also like
to recall the ``end`` keyword argument of the ``print()`` function.

Solution Output
---------------

::

  Please input a string: hello
  hello
  ellol
  lloll
  lolle
  olleh

::

  RATHER ARCHAIC MINDSET
  ATHER ARCHAIC MINDSETE
  THER ARCHAIC MINDSETES
  HER ARCHAIC MINDSETESD
  ER ARCHAIC MINDSETESDN
  R ARCHAIC MINDSETESDNI
   ARCHAIC MINDSETESDNIM
  ARCHAIC MINDSETESDNIM
  RCHAIC MINDSETESDNIM C
  CHAIC MINDSETESDNIM CI
  HAIC MINDSETESDNIM CIA
  AIC MINDSETESDNIM CIAH
  IC MINDSETESDNIM CIAHC
  C MINDSETESDNIM CIAHCR
   MINDSETESDNIM CIAHCRA
  MINDSETESDNIM CIAHCRA
  INDSETESDNIM CIAHCRA R
  NDSETESDNIM CIAHCRA RE
  DSETESDNIM CIAHCRA REH
  SETESDNIM CIAHCRA REHT
  ETESDNIM CIAHCRA REHTA
  TESDNIM CIAHCRA REHTAR

Making Change
=============

Problem
-------

Create a function that takes in a float representing a value in American
currency and determining the number of each unit (listed below) needed to
return change in the least number of bills/coins.

Include the following units:

* twenty dollar bill
* ten dollar bill
* five dollar bill
* dollar bill
* quarter
* dime
* nickle
* penny

Run your function using the following values:

* 0.23
* 6.27
* 16.05
* 23.85

Hints
-----

Your first inclination may be to create a function that takes in a float and
use division on this raw float value. Unfortunately, there are division
precision errors, so it is probably best to convert all floats to integers
and continue on.

Solution Output
---------------

::

  Making change for: 0.23
  Dime 2
  Penny 3

::

  Making change for: 6.27
  Five 1
  One 1
  Quarter 1
  Penny 2

::

  Making change for: 16.05
  Ten 1
  Five 1
  One 1
  Nickle 1

::

  Making change for: 23.85
  Twenty 1
  One 3
  Quarter 3
  Dime 1
