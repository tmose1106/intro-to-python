*******
Level 3
*******

Treasure Hunt
=============

Prerequisites
-------------

This problem requires knowledge from the appendix section `Stringing Together
Strings`_. Specifically, one should know about indexing strings. It would
also be helpful to know some of the methods available on string objects
(objects and methods are discussed in `Classifying Classes`_).

.. _`Classifying Classes`: ../tutorials/classes.html
.. _`Stringing Together Strings`: ../appendix/strings.html

Problem
-------

Suppose you are provided a table (like shown below) containing coordinates
which represent cells within the table. The coordinates are formatted such
that the tens place is the row, and the ones place is a column.

The purpose of this exercise is to parse such a table representing a treasure
map and find the single cell where the contained coordinate matches the cell
itself. At that coordinate lies thy treasure. To reach this cell, one must
start at the top left cell, read its coordinate, go to that coordinate,
check its value, rinse, repeat, profit.

::

  +------------------------+
  ¦ 34 ¦ 21 ¦ 32 ¦ 41 ¦ 25 ¦
  +----+----+----+----+----¦
  ¦ 14 ¦ 42 ¦ 43 ¦ 14 ¦ 31 ¦
  +----+----+----+----+----¦
  ¦ 54 ¦ 45 ¦ 52 ¦ 42 ¦ 23 ¦
  +----+----+----+----+----¦
  ¦ 33 ¦ 15 ¦ 51 ¦ 31 ¦ 35 ¦
  +----+----+----+----+----¦
  ¦ 21 ¦ 52 ¦ 33 ¦ 13 ¦ 23 ¦
  +------------------------+



Hints
-----

* The map will always be 5x5 table
* Each coordinate will always be a double digit
* The coordinates are all separated by a string with uniform contents
* The map can be stored inside of a multiline string
* The answer is ``52``

Solution Output
---------------

::

  Searching coordinate 11...
  No treasure at 11 :-(
  Searching coordinate 34...
  No treasure at 34 :-(
  Searching coordinate 42...
  No treasure at 42 :-(
  Searching coordinate 15...
  No treasure at 15 :-(
  Searching coordinate 25...
  No treasure at 25 :-(
  Searching coordinate 31...
  No treasure at 31 :-(
  Searching coordinate 54...
  No treasure at 54 :-(
  Searching coordinate 13...
  No treasure at 13 :-(
  Searching coordinate 32...
  No treasure at 32 :-(
  Searching coordinate 45...
  No treasure at 45 :-(
  Searching coordinate 35...
  No treasure at 35 :-(
  Searching coordinate 23...
  No treasure at 23 :-(
  Searching coordinate 43...
  No treasure at 43 :-(
  Searching coordinate 51...
  No treasure at 51 :-(
  Searching coordinate 21...
  No treasure at 21 :-(
  Searching coordinate 14...
  No treasure at 14 :-(
  Searching coordinate 41...
  No treasure at 41 :-(
  Searching coordinate 33...
  No treasure at 33 :-(
  Searching coordinate 52...
  Treasure found at 52!
