****************
Challenge Corner
****************

This section provides a series of programming challenges which will help
push the student's ability to think critically in relation to programming.
These practice problems have been inspired by (or *"barrowed"* from) several
resources listed below. However, for each challenge I have provided a custom
(commented) solution for how I solved the problem.

Many of these challenges I have modified from these resources:

* `Practice Python: Beginner Python exercises`_ by `Michele Pratusevich`_
* `Programming Practice Problems`_ by `Dr. John Dalbey`_
* `Simple Programming Problems`_ by `Adrian Neumann`_

.. _`Adrian Neumann`: https://adriann.github.io/
.. _`Dr. John Dalbey`: http://users.csc.calpoly.edu/~jdalbey/
.. _`Michele Pratusevich`: http://www.mprat.org/
.. _`Practice Python: Beginner Python exercises`: http://www.practicepython.org/
.. _`Programming Practice Problems`: http://users.csc.calpoly.edu/~jdalbey/103/Projects/ProgrammingPractice.html
.. _`Simple Programming Problems`: https://adriann.github.io/programming_problems.html

.. toctree::
  :caption: Index
  :maxdepth: 1
  :glob:

  level_*
  solutions
