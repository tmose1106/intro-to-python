***************
Intro to Python
***************

This project is dedicated to teaching the Python programming language from
front to back through concise yet intuitive examples. The site itself has been
designed as four separate parts:

* A syntax tutorial for learning the building blocks that make up the language
* Sets of sample problems which can be completed to help reinforce discussed
  topics
* Practical example mini-projects using both built-in and third-party
  utilities
* An appendix which goes into depth with topics not specific to Python


A version of the website is also available at `n2epython.netlify.com`_.

Generating the Site
===================

In order to generate the project, a few steps must be taken.

Dependencies
------------

It is suggested that all dependencies are installed within an virtual
environment using pip and the two provided requirements files
(``requirements.txt`` for build dependencies and ``requirements-dev.txt`` for
making developement easier).

General instructions for building the project are shown below. After building,
the HTML project should be available in the *build/html* directory.

Building on GNU/Linux
---------------------

::

  git clone https://gitlab.com/tmose1106/intro-to-python
  cd intro-to-python
  git submodule init
  git submodule update
  python3 -m venv env
  source env/bin/activate
  python3 -m pip install -r requirements.txt
  make html

Building on Windows
-------------------

.. note::

  This build is assuming you are using PowerShell and not Command Prompt

::

  git clone https://gitlab.com/tmose1106/intro-to-python
  cd .\intro-to-python
  git submodule init
  git submodule update
  py -m venv env
  source .\env\Scripts\Activate.ps1
  py -m pip install -r requirements.txt
  .\make.bat html

.. note::

  You must enable unsigned scripts in PowerShell to enter the virtual
  environment.

Licensing
=========

This project is released under the terms of the `Creative Commons Attribution
4.0 International`_ license. The example code is released under the terms of
the `MIT/Expat`_ license. For more information, read the *LICENSE.txt* and
*source/_static/examples/LICENSE.txt* files.

.. _`Creative Commons Attribution 4.0 International`: http://creativecommons.org/licenses/by/4.0/
.. _`MIT/Expat`: https://spdx.org/licenses/MIT.html
